package net.spottog.i18n.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Controller.RandomGenerator;
/**
 * Testet den RandomController:
 * import net.spottog.i18n.service.Controller.RandomGenerator;
 */
class RandomTest {
	private final RandomGenerator rand = Controller.getInstance().getRandom();
	/**
	 * Testet das Generieren von zufallsbytes.
	 */
	@Test
	void LengthCheckBytes() {
		for(int i = 0; i < 10000; i += 7)
		assertEquals(rand.getBytes(i).length, i);
	}
	/**
	 * Testet das Generieren von Strings verschiedener Länge.
	 */
	@Test
	void LengthCheckString() {
		for(int i = 0; i < 8000; i += 8)
		assertEquals(rand.getRandomString(i).length(), i);
	}
	/**
	 * Testet das genrieren von Zufall in den Verwendeten Längen.
	 */
	@Test
	void FixLengthCheck() {
		assertEquals(rand.getNextApiKey().length(), 64);
		assertEquals(rand.getSalt().length, 48);
	}
	


}
