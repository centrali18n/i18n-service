package net.spottog.i18n.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Controller.SecureController;
/**
 * Testet die zur verfüngstehenden Kryptografischen Funktionen.
 */
class SecureTest {

	private final SecureController sec = Controller.getInstance().getSecure();
	/**
	 * Testet das Hashen und vergleichen von Passwörtern für die Benutzer
	 */
	@Test
	void PwCheck() {
		final String[] strings = { "Hallo001535434654665", "", "eafrdtjhrzjz rff",
				"aewr e r3r³¼ŧ34tŧ43³ŧ¶ ²2 ²³ŧ¶3 ŧ←]½←³4 f³đĸ ³g¬þħ¼³½þ ø324 ³¼½þø←34 j24p 132por213 24ĸ5 ŧ¼k 3o35",
				"2", "Hallo001535434654665" };
		String last = "";
		for (String s : strings) {
			assertTrue(sec.checkPasswd(s, sec.generatePW(s)));
			assertFalse(sec.checkPasswd(last, sec.generatePW(s)));
			assertNotEquals(s, sec.generatePW(s));
			last = s;
		}
	}
	/**
	 * Testet die Base 64 Funktionen
	 */
	@Test
	void base64check() {
		assertEquals("Hallo001535434654665", sec.Encode(sec.Decode("Hallo001535434654665")));
	}
}
