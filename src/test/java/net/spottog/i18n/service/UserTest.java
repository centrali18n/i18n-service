package net.spottog.i18n.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Model.User;

class UserTest {

	@Test
	void CheckUserFixData() {
		final String pw1 = "test";
		final String pw2 = "Test";
		final String email = "test@example.com";
		final int id = 0;
		final User user = new User(id, email, Controller.getInstance().getSecure().generatePW(pw1));
		assertTrue(user.CheckPasswd(pw1));
		assertFalse(user.CheckPasswd(pw2));
		assertNotEquals(pw2 ,user.ChangePasswd(pw2));
		assertTrue(user.CheckPasswd(pw2));
		assertFalse(user.CheckPasswd(pw1));
		assertEquals(email, user.getEmail());
		assertEquals(id, user.getID());
	}

}
