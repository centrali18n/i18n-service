<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.AbstractSet"%>
<%@page import="net.spottog.i18n.service.Model.ApiKeyValue"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%
	String apikey = request.getParameter("param");
	String keycontent = "";
	String onchangenotify = "";
	if(apikey != null && apikey.trim().length() > 0){
		keycontent = " value=\"" + apikey + "\" disabled=\"true\"";
	}
%>
<h2><%=new I18n("apitest").toString(request.getLocale())%></h2>

		<form action="#"
			onsubmit="testApiLoadText($('#apiKey').val(), $('#searchString').val(), $('#language').val()); return false;">
			<div class="form-group">
				<label for="apiKey"><%=new I18n("apikey").toString(request.getLocale())%></label>
				<input type="text" class="form-control" id="apiKey"<%=keycontent %> onkeypress="testApiTextChange()" oncut="testApiTextChange()" onpaste="testApiTextChange()">
			</div>
			<div class="form-group">
				<label for="searchString"><%=new I18n("searchString").toString(request.getLocale())%></label>
				<input type="text" class="form-control" id="searchString"  onkeypress="testApiTextChange()" oncut="testApiTextChange()" onpaste="testApiTextChange()">
			</div>
			<div class="form-group">
				<label for="language"><%=new I18n("language").toString(request.getLocale())%></label>
				<input type="text" class="form-control" id="language" onkeypress="testApiTextChange()" oncut="testApiTextChange()" onpaste="testApiTextChange()">
			</div>
			<button type="submit" class="btn btn-default"><%=new I18n("send").toString(request.getLocale())%></button>
		</form>
		<br />
<div id=""></div><br />
<div class="card">
  <div class="card-header"><%=new I18n("answer").toString(request.getLocale())%></div>
  <div class="card-body scrollcontent" id="secoundcontent"></div>
</div>
<br />
<div class="card">
  <div class="card-header"><%=new I18n("curl").toString(request.getLocale())%></div>
  <div class="card-body"><small  id="thirdcontent"></small></div>
</div>
