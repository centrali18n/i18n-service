<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@page import="java.util.Locale"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.service.Model.ExpireText"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="java.util.Map"%>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null)
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
	int SaveID = Integer.parseInt(request.getParameter("saveid"));
	TextController texte = TextController.Get(SaveID);
	String[] languages = request.getParameter("languages").split(",");
	for (String lang : languages) {
		if (!user.hasRight(UserRight.readText, lang, SaveID))
	Controller.getInstance()
			.IllegalUnauthorizedAccessError(new I18n(lang, new I18n(" ", new I18n("Not Allowed"))));
	}
	Map<String, Map<String, ExpireText>> map = texte.get3DMapFor(languages);
	final ExpireText DEFAULT = new ExpireText("", 0);
%>
<h2><%=texte.getName()%></h2>
<div class="table-responsive">
	<table class="table table-hover table-striped table-bordered sortable"
		id="listTable" style="width: 100%">
		<thead class="thead-dark">
			<tr>
				<th  scope="col"><%=new I18n("SearchString").toString(request.getLocale())%></th>
				<%
					int i=1;
					for (String lang : languages) {
				%>
				<th data-field="SearchString<%=lang%>" data-sortable="true"
					scope="col" data-editable="true"><%=new Locale(lang).getDisplayLanguage(request.getLocale())%></th>
				<%
					}
				%>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-clock-o"></i></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-save"></i></th>
				<%if(user.hasRight(UserRight.editUser, null, SaveID)){ %>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-trash" title="<%=new I18n("textsave", new I18n(" ", new I18n("delete"))) %> <%=texte.getName()%>" onclick="deleteTextSave('<%=SaveID %>')"></i></th>
				<%}else{ %>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-trash"></i></th>
				<%} %>
			</tr>
		</thead>
		<tbody>
			<%
				int row = 0;
				for (Map.Entry<String, Map<String, ExpireText>> searchstrings : texte.get3DMapFor(languages).entrySet()) {
					row++;
			%>
			<tr>
				<td scope="row"><%=searchstrings.getKey()%></td>
				<%
					for (String lang : languages) {
							ExpireText et = searchstrings.getValue().getOrDefault(lang, DEFAULT);
				%>
				<td title="<%=et.getExpireTime()%>" contenteditable="true" onkeypress="tableCellChanged(event)" oncut="tableCellChanged(event)" onpaste="tableCellChanged(event)"><%=et.getText()%></td>
				<%
					}
				%>
				<td scope="col"><a class="fa fa-clock-o"
					onclick="tableRowEdit(event)" id="timetext<%=row %>"></a></td>
				<td scope="col"><a class="fa fa-save"
					onclick="tableRowSave(event, '<%=row %>')"></a></td>
				<td scope="col"><a class="fa fa-trash"
					onclick="tableRowDelete(event)"></a></td>
			<tr>
				<%
					}
				%>
			
		</tbody>
	</table>
</div>
<div id="toAddButton">
	<span class="table-add float-right mb-3 mr-2"><a href="#!"
		onclick="tableNewElement('<%=texte.getDefaultExpireTime() %>', '<%=++row %>')" class="text-success" id="toAddButtonSpan"><i
			class="fa fa-plus-square"></i></a></span>
</div>
<div id="secoundcontent"></div>
<div id="backtoOverview">
	<a onclick="loadselectlist()"><%=new I18n("back to overview").toString(request.getLocale())%></a>
</div>
