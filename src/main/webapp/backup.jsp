<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.AbstractSet"%>
<%@page import="net.spottog.i18n.service.Model.ApiKeyValue"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@page import="net.spottog.i18n.service.Model.UserRight"%>

<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);;
	if(user == null || !user.hasRight(UserRight.backup, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("Backup").toString(request.getLocale())%></h2>
<h5><%=new I18n("Under Construction").toString(request.getLocale())%></h5>
<div id="secoundcontent"></div>
