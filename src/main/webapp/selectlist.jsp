<%@page import="java.util.Collection"%>
<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@page import="java.util.Locale"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null)
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
	Collection<Locale> localcollection = user.getLocaleList();
%>
<h2><%=new I18n("listdescription").toString(request.getLocale())%></h2>
<%
	//Collection<TextController> list = user.getTextControllerList();
	for (TextController tc : user.getTextControllerList()) {
%>
<a class="dropdown-item" href="#" onclick='loadList(<%=tc.getID()%>)'>(<%=tc.getID()%>) <%=tc.getName()%></a>
<%
	}
%>
<br />
<br />
<div class="dropup">
	<button class="btn btn-black dropdown-toggle btn-block" type="button"
		data-toggle="dropdown"><%=new I18n("selectlanguage").toString(request.getLocale())%>
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<%
			for (Locale lc : localcollection) {
		%>
		<li><a class="dropdown-item LocaleSelector fa"
			id="LocaleSelectorinselect<%=lc.getLanguage()%>"
			onclick='laguageclicked(event, "<%=lc.getLanguage()%>")'><%=lc.getDisplayLanguage(request.getLocale())%></a></li>
		<%
			}
		%>
	</ul>
</div>
<br />
<div id="secoundcontent"></div>
<script type="text/javascript">
$( document ).ready(function() {
	<%for (Locale lc : localcollection) {
				final String namePostfix = lc.getLanguage();%>
if ($("#LocaleSelector<%=namePostfix%>").hasClass("fa-check")) {
	$("#LocaleSelectorinselect<%=namePostfix%>").addClass("fa-check");
} else {
	$("#LocaleSelectorinselect<%=namePostfix%>").addClass("fa-close");
}
<%}%>});
</script>