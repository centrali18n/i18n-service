<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="48x48" href="favicon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="site.webmanifest">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<link rel="stylesheet" href="css/site.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.2/bootstrap-table.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script
	src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link
	href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"
	rel="stylesheet">
<script src="js/net.spottog.i18n.js"></script>
<script src="js/site.js"></script>
<script src="js/sorttable.js"></script>
<title><%=new I18n("title").toString(request.getLocale())%></title>
<script type="text/javascript">
SiteLinkPrefix = "<%=Controller.getInstance().getConfig("SiteLinkPrefix")%>";
SiteHostname = "<%=Controller.getInstance().getConfig("SiteLinkHostname")%>";
i18nInit("<%=request.getLocale()%>", "rest/text/","jNBFsgSvQeqr_UmmSGhXVpewd_TwAwJDlE_DDu8vA7Rf4CT9RJJxbCiywYWjetAV");
</script>

</head>
<body>
	<nav
		class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top navbar-inverse">
		<!-- Brand -->
		<a class="navbar-brand" href="#"><%=new I18n("title").toString(request.getLocale())%></a>
		<div class="collapse navbar-collapse" id="navbarText">
			<!-- Links -->
			<ul class="navbar-nav">
			</ul>
		</div>
		<ul class="navbar-nav navbar-inline">
			<li id="LoginText"><a class="nav-link"
				href="<%=Controller.getInstance().getConfig("SiteLinkPrefix") %>">Login</a></li>
		</ul>
	</nav>
	<br />
	<div class="container" id="content">
		<p><%=new I18n("greeting").toString(request.getLocale())%></p>
		<form action="#"
			onsubmit="UserLogin($('#email').val(), $('#passwd').val()); return false;">
			<div class="form-group">
				<label for="email"><%=new I18n("Email", new I18n(" ", new I18n("address", new I18n(":")))).toString(request.getLocale())%></label>
				<input type="email" class="form-control" id="email">
			</div>
			<div class="form-group">
				<label for="passwd"><%=new I18n("password", new I18n(":")).toString(request.getLocale())%></label>
				<input type="password" class="form-control" id="passwd">
			</div>
			<button type="submit" class="btn btn-default"><%=new I18n("login").toString(request.getLocale())%></button>
		</form>
	</div>
</body>
</html>
<% String abfrage = request.getParameter("load");
	if(abfrage != null && abfrage.trim().length() > 0){
		String loadSite = null;
		if(abfrage.toLowerCase().equals("test")){
			%>
			<script type="text/javascript">
			loadTest('');
			</script>
			<%
		}
	}
%>
