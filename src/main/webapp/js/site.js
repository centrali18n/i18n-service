var MyUserObject;
function userGetEmail() {
	return MyUserObject.email;
}
function userLogout() {
	$.get("rest/user/logout/", function() {
		return true;
	}).always(function() {
		MyUserObject = null;
	});
	return false;
}
function criticalerror(text, code, msg) {
	if (!(!MyUserObject || MyUserObject == undefined)) {
		$.get("rest/user/logout/" + userGetTokken(), function() {
		}).always(function() {
			MyUserObject = null;
		});
	}
	var ErrorText = "(" + code + ") " + msg + '\n' + text;
	$("#content").html(ErrorText);
	alert(ErrorText);
	window.location = SiteLinkPrefix;
}
function UserLogin(email, passwd) {
	var UserRequest = {
		email : email,
		passwd : passwd
	};
	$("#content").html("login");
	$.ajax({
		url : "rest/user/",
		data : JSON.stringify(UserRequest),
		contentType : 'application/json',
		type : 'POST',
		success : function(data) {
			MyUserObject = data;
			$("#LoginText").html(
					'<a class="nav-link" href="' + SiteLinkPrefix
							+ '" onclick="userLogout();">' + userGetEmail()
							+ '</a>');
			$("#navbarText").load("menu.jsp");
			loadselectlist();
		},
		error : function(data) {
			criticalerror(data.responseText, data.status, data.statusText);
		}
	});
	return false;
}
var loadedList = null;
function loadselectlist() {
	loadedList = null;
	$("#content").load("selectlist.jsp");
}
var SiteLinkPrefix;
var SiteHostname;
function loadUserConfig() {
	loadedList = null;
	$("#content").load("userconfig.jsp");
}
function loadBackup() {
	loadedList = null;
	$("#content").load("backup.jsp");
}
function loadTest(apikey) {
	loadedList = null;
	$("#content").load("testapi.jsp?param=" + apikey, function() { testApiTextChange(); });
}
function loadApiConfig() {
	loadedList = null;
	$("#content").load("apiconfig.jsp", function() {
		LoadSecoundContent('NewApiKey');
		sorttable.makeSortable(document.getElementById("listTable"));
	});
}
function laguageclicked(event, item) {
	event.stopPropagation();
	var menuitem = $("#LocaleSelector" + item);
	if (menuitem.hasClass("fa-check")) {
		menuitem.removeClass("fa-check");
		menuitem.addClass("fa-close");
	} else {
		menuitem.removeClass("fa-close");
		menuitem.addClass("fa-check");
	}
	var selectitem = $("#LocaleSelectorinselect" + item);
	if (!(!selectitem || selectitem == undefined)) {
		if (selectitem.hasClass("fa-check")) {
			selectitem.removeClass("fa-check");
			selectitem.addClass("fa-close");
		} else {
			selectitem.removeClass("fa-close");
			selectitem.addClass("fa-check");
		}
	}
	if (loadedList != null)
		loadList(loadedList);
	return false;
}
function loadList(list) {
	loadedList = list;
	var languages = null;
	$(".LocaleSelector.LocalList.fa-check").each(function() {
		if (languages == null) {
			languages = "";
		} else {
			languages += ",";
		}
		languages += $(this).attr('id').substr(14);
	});
	$("#content").load("list.jsp?saveid=" + list + "&languages=" + languages,
			function() {
				sorttable.makeSortable(document.getElementById("listTable"));
			});
}
function setTableCellToEditable(cell) {
	cell.setAttribute('contenteditable', "true");
	cell.setAttribute('onkeypress', "tableCellChanged(event)");
	cell.setAttribute('oncut', "tableCellChanged(event)");
	cell.setAttribute('onpaste', "tableCellChanged(event)");
}
function tableNewElement(expireTime, nextid) {
	document.getElementById('toAddButtonSpan').setAttribute("onClick",
			'tableNewElement("' + expireTime + '", "' + (nextid + 1) + '")');
	var table = document.getElementById("listTable");
	var row = table.insertRow();
	var cell1 = row.insertCell();
	cell1.setAttribute('scope', "row");
	cell1.innerHTML = "new";
	setTableCellToEditable(cell1);
	$(".LocaleSelector.LocalList.fa-check").each(function() {
		var cell = row.insertCell();
		setTableCellToEditable(cell);
		cell.setAttribute("title", expireTime);
	});
	var cellclock = row.insertCell();
	cellclock.innerHTML = '<td scope="col"><a class="fa fa-clock-o" onclick="tableRowEdit(event)" id="timetext'
			+ nextid + '"></a></td>';
	var cellsave = row.insertCell();
	cellsave.innerHTML = '<td scope="col"><a class="fa fa-save"></a></td>';
	var celtrash = row.insertCell();
	cellsave.onclick = function(event) {
		tableRowSave(event, nextid);
		cellsave.setAttribute('onclick', 'tableRowSave(event, "' + nextid
				+ '")');
		celtrash.innerHTML = '<td scope="col"><a class="fa fa-trash" onclick="tableRowDelete(event)"></a></td>';
	}
}
function tableRowSave(e, timetext) {
	e = e || window.event;
	e = e.target || e.srcElement;
	row = e.parentNode.parentNode;
	// now is e the icon row the caller row
	e.classList.remove("fa-save");
	e.classList.add("fa-spinner");
	row.classList.remove("table-primary");
	timetext = document.getElementById("timetext" + timetext);
	if (timetext.classList.contains("fa-pencil-square-o")) {
		timetext.click();
	}
	row.cells[0].setAttribute('contenteditable', "false");
	var i = 1;
	var searchstring = row.cells[0].innerHTML
	var toEdit = new Array();
	$(".LocaleSelector.LocalList.fa-check").each(function() {
		var language = $(this).attr('id').substr(14);
		var cell = row.cells[i++];
		var text = cell.innerHTML.replace('<br>', '').trim();
		var expire = cell.getAttribute("title");
		if (!(!text || text == undefined || text == ""))
			toEdit.push({
				language : language,
				text : text,
				expire : expire
			})
	});
	var toSave = JSON.stringify({
		searchstring : searchstring,
		saveid : loadedList,
		toedit : toEdit
	});
	$
			.ajax({
				url : "rest/text/edit/",
				data : toSave,
				contentType : 'application/json',
				type : 'POST',
				success : function() {
					e.classList.remove("fa-spinner");
					e.classList.add("fa-save");
					row.cells[row.cells.length - 1].innerHTML = '<a class="fa fa-trash" onclick="tableRowDelete(event)"></a>';
				}
			});
	console.log("Save:\n" + toSave);
}
function tableRowDelete(e) {
	e = e || window.event;
	e = e.target || e.srcElement;
	row = e.parentNode.parentNode;
	// now is e the icon row the caller row
	e.classList.remove("fa-save");
	e.classList.add("fa-spinner");
	var DeleteText = JSON.stringify({
		searchstring : row.cells[0].innerHTML,
		saveid : loadedList
	});
	$.ajax({
		url : "rest/text/edit/",
		data : DeleteText,
		contentType : 'application/json',
		type : 'DELETE',
		success : function() {
			e.classList.remove("fa-spinner");
			e.classList.add("fa-save");
			row.parentNode.parentNode.deleteRow(row.rowIndex);
		}
	});
	console.log("Delete:\n" + DeleteText);
}

function newApiKey(saveid) {
	var ApiKeyRequest = {
		textsaveid : saveid
	};
	// build table
	var table = document.getElementById("listTable");
	var row = table.insertRow();
	// cellApiKey = document.createElement('th');
	var cellApiKey = row.insertCell();
	cellApiKey.setAttribute('scope', "row");
	cellApiKey.innerHTML = 'new';
	// row.appendChild(cellApiKey);
	var cellsort = row.insertCell();
	cellsort.setAttribute('scope', "col");
	var cellsaveid = row.insertCell();
	cellsaveid.innerHTML = saveid;
	cellsaveid.setAttribute('scope', "col");
	var cellsavename = row.insertCell();
	cellsavename.setAttribute('scope', "col");
	var cellsave = row.insertCell();
	cellsave.setAttribute('scope', "col");
	cellsave.innerHTML = '<a class="fa fa-plus-square"></a>';
	var celltest = row.insertCell();
	celltest.setAttribute('scope', "col");
	celltest.innerHTML = '<a class="fa fa-play"></a>';
	var celtrash = row.insertCell();
	celtrash.setAttribute('scope', "col");
	celtrash.innerHTML = '<a class="fa fa-trash" onclick="tableRowDelete(event)"></a>';
	// ajax
	$.ajax({
				url : "rest/ApiKey/",
				data : JSON.stringify(ApiKeyRequest),
				contentType : 'application/json',
				type : 'POST',
				success : function(data) {
					console.log("new api key: " + data);
					cellApiKey.innerHTML = data.apikey;
					cellsort.innerHTML = data.sort;
					cellsaveid.innerHTML = data.saveid;
					cellsavename.innerHTML = data.listname;
					celltest.innerHTML = '<a class="fa fa-play" onclick="loadTest(&quot;' + data.apikey + '&quot;)"></a>';
					cellsave.innerHTML = '<a class="fa fa-plus-square" onclick="LoadSecoundContent(&quot;AddToApiKey&quot;, &quot;'	+ data.apikey + '&quot;)"></a>';
				},
				error : function() {
					alert("Error");
				}
			});
}
var lastEditEditButton = null;
function resetLastEditEditButton() {
	if (lastEditEditButton || lastEditEditButton != undefined) {
		lastEditEditButton.classList.replace("fa-level-down", "fa-plus-square");
	}
}
function editApitKey(e, apikey) {
	LoadSecoundContent('AddToApiKey', apikey);
	resetLastEditEditButton();
	e = e || window.event;
	e = e.target || e.srcElement;
	e.classList.replace("fa-plus-square", "fa-level-down");
	lastEditEditButton = e;
}
function addToApiKey(apikey, saveid, sort) {
	var ApiKeyRequest = {
		apikey : apikey,
		textsaveid : saveid,
		sort : sort
	};
	// build table
	var table = document.getElementById("listTable");
	var row = table.insertRow();
	// cellApiKey = document.createElement('th');
	var cellApiKey = row.insertCell();
	cellApiKey.setAttribute('scope', "row");
	cellApiKey.innerHTML = 'new';
	// row.appendChild(cellApiKey);
	var cellsort = row.insertCell();
	cellsort.setAttribute('scope', "col");
	var cellsaveid = row.insertCell();
	cellsaveid.innerHTML = saveid;
	cellsaveid.setAttribute('scope', "col");
	var cellsavename = row.insertCell();
	cellsavename.setAttribute('scope', "col");
	var cellsave = row.insertCell();
	cellsave.setAttribute('scope', "col");
	cellsave.innerHTML = '<a class="fa fa-plus-square"></a>';
	var celltest = row.insertCell();
	celltest.setAttribute('scope', "col");
	celltest.innerHTML = '<a class="fa fa-play"></a>';
	var celtrash = row.insertCell();
	celtrash.setAttribute('scope', "col");
	celtrash.innerHTML = '<a class="fa fa-trash" onclick="tableRowDelete(event)"></a>';
	// ajax
	$
			.ajax({
				url : "rest/ApiKey/",
				data : JSON.stringify(ApiKeyRequest),
				contentType : 'application/json',
				type : 'PUT',
				success : function(data) {
					console.log("add to api key: " + data);
					cellApiKey.innerHTML = data.apikey;
					cellsort.innerHTML = data.sort;
					cellsaveid.innerHTML = data.saveid;
					cellsavename.innerHTML = data.listname;
					celltest.innerHTML = '<a class="fa fa-play" onclick="loadTest(&quot;' + data.apikey + '&quot;)"></a>';
					cellsave.innerHTML = '<a class="fa fa-plus-square" onclick="LoadSecoundContent(&quot;AddToApiKey&quot;, &quot;'	+ data.apikey + '&quot;)"></a>';
				},
				error : function() {
					alert("Error");
				}
			});
	resetLastEditEditButton();
}
function ApiKeyDelete(e) {
	e = e || window.event;
	e = e.target || e.srcElement;
	row = e.parentNode.parentNode;
	// now is e the icon row the caller row
	e.classList.remove("fa-save");
	e.classList.add("fa-spinner");
	var DeleteApiKey = JSON.stringify({
		apikey : row.cells[0].innerHTML,
		textsaveid : row.cells[2].innerHTML
	});
	$.ajax({
		url : "rest/ApiKey/",
		data : DeleteApiKey,
		contentType : 'application/json',
		type : 'DELETE',
		success : function() {
			e.classList.remove("fa-spinner");
			e.classList.add("fa-save");
			row.parentNode.parentNode.deleteRow(row.rowIndex);
			console.log("Delete:\n" + DeleteApiKey);
		},
		error : function(data) {
			alert("Error" + data);
		}
	});
}
function LoadSecoundContent(file, param) {
	$("#secoundcontent").load("secoundcontent/" + file + ".jsp?param=" + param);
}
function addLanguage() {
	LoadSecoundContent("NewLanguage", loadedList);
}
function addTextTable() {
	LoadSecoundContent("NewTextTable", loadedList);
}
function newLanguage(language, saveid) {
	$("#secoundcontent").html("");
	var toEdit = new Array();
	var toSave = JSON.stringify({
		searchstring : " ",
		saveid : saveid,
		toedit : new Array({
			language : language,
			text : " "
		})
	});
	$.ajax({
		url : "rest/text/edit/",
		data : toSave,
		contentType : 'application/json',
		type : 'POST',
		success : function() {
			$("#navbarText").load("menu.jsp");
			loadselectlist();
		}
	});
}
function tableRowEdit(e) {
	e = e || window.event;
	e = e.target || e.srcElement;
	row = e.parentNode.parentNode;
	// now is e the icon row the caller row
	if (e.classList.contains("fa-pencil-square-o")) {
		e.classList.replace("fa-pencil-square-o", "fa-clock-o");
	} else {
		e.classList.replace("fa-clock-o", "fa-pencil-square-o");
	}
	// row.appendChild(cell1);
	var i = 1;
	$(".LocaleSelector.LocalList.fa-check").each(function() {
		var cell = row.cells[i++];
		var alt = cell.getAttribute("title");
		cell.setAttribute("title", cell.innerHTML);
		cell.innerHTML = alt;
	});
}
function tableCellChanged(event) {
	event.target.parentNode.classList.add("table-primary");
}
function newTextTable(tablename, DefaultExpire) {
	var NewTextSaveTrasport = {
		name : tablename,
		defaultexpire : DefaultExpire
	};
	$("#secoundcontent").html("");
	$.ajax({
		url : "rest/text/newSave/",
		data : JSON.stringify(NewTextSaveTrasport),
		contentType : 'application/json',
		type : 'POST',
		success : function(newSaveID) {
			$("#navbarText").load("menu.jsp");
			loadList(newSaveID);
		}
	});
}
function deleteTextSave(SaveID) {
	$.ajax({
		url : "rest/text/delSave/",
		data : SaveID,
		contentType : 'application/json',
		type : 'DELETE',
		success : function() {
			$("#navbarText").load("menu.jsp");
			loadselectlist();
		},
		error : function(data) {
			alert("(" + data.status + ") " + data.statusText + '\n'
					+ data.responseText);
		}
	});
	console.log("Delete SaveID: " + SaveID);
}
var lastEditUserButton = null;
var lastEditUserPWButton = null;
function resetLastEditUserButton() {
	if (lastEditUserButton || lastEditUserButton != undefined) {
		lastEditUserButton.classList.replace("fa-level-down",
				"fa-pencil-square-o");
	}
	if (lastEditUserPWButton || lastEditUserPWButton != undefined) {
		lastEditUserPWButton.classList.replace("fa-level-down", "fa-key");
	}
}
function EditUser(e, id) {
	LoadSecoundContent('EditUser', id);
	resetLastEditUserButton();
	e = e || window.event;
	e = e.target || e.srcElement;
	e.classList.replace("fa-pencil-square-o", "fa-level-down");
	lastEditUserButton = e;
}
function EditUserPW(e, id) {
	LoadSecoundContent('EditUserPW', id);
	resetLastEditUserButton();
	e = e || window.event;
	e = e.target || e.srcElement;
	e.classList.replace("fa-key", "fa-level-down");
	lastEditUserPWButton = e;
}
function DeleteUser(event, id) {
	$.ajax({
		url : "rest/user/",
		data : id,
		contentType : 'text/plain',
		type : 'DELETE',
		success : function(data) {
			$("#content").load("userconfig.jsp");
		},
		error : function(data) {
			alert("(" + data.status + ") " + data.statusText + '\n'
					+ data.responseText);
		}
	});
}
function addNewUser(email, password1, password2) {
	if (password1 == password2) {
		var UserRequest = {
			email : email,
			passwd : password1
		};
		$.ajax({
			url : "rest/user/",
			data : JSON.stringify(UserRequest),
			contentType : 'application/json',
			type : 'PUT',
			success : function(data) {
				$("#content").load("userconfig.jsp?markedit=" + data.ID,
						function(data) {
							LoadSecoundContent('EditUser', data.ID);
						})
			},
			error : function(data) {
				alert("(" + data.status + ") " + data.statusText + '\n'
						+ data.responseText);
			}
		});
	}
}
function SetUserPW(id, password1, password2) {
	if (password1 == password2) {
		var UserPasswdRequest = {
			userid : id,
			passwd : password1
		};
		$.ajax({
			url : "rest/user/setpasswd",
			data : JSON.stringify(UserPasswdRequest),
			contentType : 'application/json',
			type : 'Post',
			success : function() {
				LoadSecoundContent('EditUserPwSuccess', id);
				resetLastEditUserButton();
			},
			error : function(data) {
				alert("(" + data.status + ") " + data.statusText + '\n'
						+ data.responseText);
			}
		});
	}
}
function userRightChange(userid, newvalue, userright){
	var UserRightChangeRequest = {
			userid : userid,
			userright : userright
	}
	$.ajax({
		url : "rest/user/" + (newvalue ? "adduserright" : "removeuserright"),
		data : JSON.stringify(UserRightChangeRequest),
		contentType : 'application/json',
		type : 'Post',
		error : function(data) {
			LoadSecoundContent('EditUser', userid);
			alert("(" + data.status + ") " + data.statusText + '\n'
					+ data.responseText);
		}
	});
}
function addRightToUser(userid, saveid){
	sendUserRightAddRequest(userid, 'readText', saveid, null,
			function(){
		LoadSecoundContent('EditUser', userid);
	},
	function(data){
		LoadSecoundContent('EditUser', userid);
		alert("(" + data.status + ") " + data.statusText + '\n'
				+ data.responseText);
	});
}
function removeUserRightForTextSave(userid, textsaveid, lang){
	lang = (lang || lang != undefined) ? null : lang;
	sendUserRightDelRequest(userid, 'readText', textsaveid, lang,
			function(){
		LoadSecoundContent('EditUser', userid);
	},
	function(data){
		LoadSecoundContent('EditUser', userid);
		alert("(" + data.status + ") " + data.statusText + '\n'
				+ data.responseText);
	});
	return false;
}
function sendUserRightAddRequest(userid, userright, saveid, language, success, error){
	var UserRightChangeRequest = {
			userid : userid,
			userright : userright,
			saveid : saveid,
			language : language,
	}
	$.ajax({
		url : "rest/user/adduserright",
		data : JSON.stringify(UserRightChangeRequest),
		contentType : 'application/json',
		type : 'Post',
		success : success,
		error : error
	});
}
function sendUserRightDelRequest(userid, userright, saveid, language, success, error){
	var UserRightChangeRequest = {
			userid : userid,
			userright : userright,
			saveid : saveid,
			language : language,
	}
	$.ajax({
		url : "rest/user/removeuserright",
		data : JSON.stringify(UserRightChangeRequest),
		contentType : 'application/json',
		type : 'Post',
		success : success,
		error : error
	});
}

function userRightTextSaveAllLangChange(userid, textsaveid, newvalue, writeaccess){
	function errorfunct(data){
		LoadSecoundContent('EditUser', userid);
		alert("(" + data.status + ") " + data.statusText + '\n'
				+ data.responseText);
	}
	sendUserRightDelRequest(userid, writeaccess ? 'writeText' : 'readText', textsaveid, newvalue, function(){
		sendUserRightAddRequest(userid, writeaccess ? 'writeText' : 'readText', textsaveid, newvalue,
				function(){
			LoadSecoundContent('EditUser', userid);
		},
		errorfunct);
	}, errorfunct);
}
function userRightChangeTextWrite(userid, newvalue, id, locale){
	function success(){
		LoadSecoundContent('EditUser', userid);
	};
	function error(data){
		LoadSecoundContent('EditUser', userid);
		alert("(" + data.status + ") " + data.statusText + '\n'
				+ data.responseText);
	}
	if(newvalue){
		sendUserRightAddRequest(userid, 'writeText', id, locale, success, error);
	}else{
		sendUserRightDelRequest(userid, 'writeText', id, locale, success, error);
	}
}
function testApiLoadText(apikey, searchString, language){
	function printit(data){
		var finish = new Date();
		document.getElementById("secoundcontent").insertAdjacentElement('afterbegin',document.createElement('pre')).innerHTML = JSON.stringify(data, null, 2);
		document.getElementById("secoundcontent").insertAdjacentElement('afterbegin',document.createElement('pre')).innerHTML = finish + " : " + (finish - send) + "ms";
	}
	var send = new Date();
	var TextWithLanguageRequest = {
			apiKey : apikey,
			searchString : searchString,
			language : language
	}
	$.ajax({
		url : "rest/text",
		data : JSON.stringify(TextWithLanguageRequest),
		contentType : 'application/json',
		type : 'Post',
		success : printit,
		error : printit
	});	
}
function testApiTextChange(){
	document.getElementById("thirdcontent").innerHTML = "curl -X POST -H 'Content-Type: application/json' -i "+SiteHostname+SiteLinkPrefix + "rest/text --data '{\"apiKey\":\""+$('#apiKey').val()+"\",\"searchString\":\""+$('#searchString').val()+"\",\"language\":\""+$('#language').val()+"\"}'";
}



