var i18nAddress;
var i18nApiKey;
var i18nLanguage;

function i18nInit(Language, Address, Apikey) {
	i18nLanguage = Language;
	i18nAddress = Address;
	i18nApiKey = Apikey;
}
function i18n(text, texttoAdd) {
	// recurrent call at chaining
	if (!(!texttoAdd || texttoAdd == undefined)) {
		return i18n(text) + texttoAdd;
	}
	console.log("Search Text " + text);
	// Found in Local Save
	if (typeof (Storage) !== "undefined") {
		var i18ntext = localStorage.getItem("net.spottog.i18n" + text);
		if (!(!i18ntext || i18ntext == undefined)) {
			i18ntext = JSON.parse(i18ntext);
			if (i18ntext.Expire < new Date())
				return i18ntext;
		}
	}
	// not found and no init
	if ((!i18nAddress || i18nAddress == undefined)) {
		console.log("Please init i18n.spottog.net, see the docs.")
		return text;
	}
	var TextWithLanguageRequest = {
		searchString : text,
		language : i18nLanguage
	};
	if (!(!i18nApiKey || i18nApiKey == undefined)) {
		TextWithLanguageRequest.apiKey = i18nApiKey;
	}
	$.ajax({
		url : i18nAddress,
		data : JSON.stringify(TextWithLanguageRequest),
		async: true,
		contentType : 'application/json',
		type : 'POST',
		success : function(data) {
			localStorage.setItem("net.spottog.i18n" + text, JSON
					.stringify(data));
			return data.Text;
		},
		error : function() {
			console.log("Error i18n Request");
			return text;
		}
	});
}
