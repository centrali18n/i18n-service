<%@page import="java.util.Collection"%>
<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@page import="java.util.Locale"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null || !user.hasRight(UserRight.editUser, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("userconfig").toString(request.getLocale())%></h2>
<div class="table-responsive">
	<table class="table table-hover table-striped table-bordered sortable" id="listTable" style="width: 100%">
		<thead class="thead-dark">
			<tr>
				<th scope="col"><%=new I18n("ID").toString(request.getLocale())%></th>
				<th scope="col"><%=new I18n("email").toString(request.getLocale())%></th>
				<th scope="col" title="<%=new I18n("editUser").toString(request.getLocale())%>"><i class="fa fa-users"></i></th>
				<th scope="col" title="<%=new I18n("editkeys").toString(request.getLocale())%>"><i class="fa fa-key"></i></th>
				<th scope="col"><%=new I18n("textRights").toString(request.getLocale())%></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-pencil-square-o"></i></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-key" title="<%=new I18n("editPassword").toString(request.getLocale())%>"></i></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-trash"></i></th>
			</tr>
		</thead>
		<tbody>
			<%
				
					for (User u : Controller.getInstance().getUserController().getList()) {
			%>
			<tr>
				<td scope="row"><%=u.getID() %></td>
				<td scope="col"><%=u.getEmail() %></td>
				<td scope="col" title="<%=(u.hasRight(UserRight.editUser, null, 0) ? new I18n("hasrightedituser") : new I18n("hasnotrightedituser")).toString(request.getLocale()) %>"><i class="fa <%=(u.hasRight(UserRight.editUser, null, 0) ? "fa-user-plus" : "fa-user-times") %>"></i></td>
				<td scope="col" title="<%=(u.hasRight(UserRight.editKeys, null, 0) ? new I18n("hasrighteditkeys") : new I18n("hasnotrighteditkeys")).toString(request.getLocale()) %>"><i class="fa <%=(u.hasRight(UserRight.editKeys, null, 0) ? "fa-book" : "fa-circle-o-notch") %>"></i></td>			
				<td scope="col"><%=u.getLocaleList().size() %></td>
				<td scope="col" ><a class="fa fa-pencil-square-o" onclick="EditUser(event, '<%=u.getID() %>')"></a></td>
				<td scope="col" ><a class="fa fa-key" title="<%=new I18n("editPassword").toString(request.getLocale())%>" onclick="EditUserPW(event, '<%=u.getID() %>')"></a></td>
				<td scope="col"><a class="fa fa-trash" onclick="DeleteUser(event, '<%=u.getID() %>')"></a></td>
			<tr>
				<%
					}
				%>
			
		</tbody>
	</table>
	<br />
</div>
<div id="secoundcontent">
	<span class="table-add float-right mb-3 mr-2"><a href="#!"
		onclick="LoadSecoundContent('NewUser')" class="text-success" id="toAddButtonSpan"><i class="fa fa-plus-square"></i></a></span>
</div>
