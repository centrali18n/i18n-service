<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.AbstractSet"%>
<%@page import="net.spottog.i18n.service.Model.ApiKeyValue"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@page import="net.spottog.i18n.service.Model.UserRight"%>

<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);;
	if(user == null || !user.hasRight(UserRight.editKeys, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("apiconfig").toString(request.getLocale())%></h2>
<div class="table-responsive">
	<table class="table table-hover table-striped table-bordered sortable" id="listTable" style="width: 100%">
		<thead class="thead-dark">
			<tr>
				<th scope="col"><%=new I18n("apikey").toString(request.getLocale())%></th>
				<th scope="col"><%=new I18n("sort").toString(request.getLocale())%></th>
				<th scope="col"><%=new I18n("saveid").toString(request.getLocale())%></th>
				<th scope="col"><%=new I18n("name").toString(request.getLocale())%></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-plus-square"></i></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-play"></i></th>
				<th class="sorttable_nosort" scope="col" style="width: 7px"><i class="fa fa-trash"></i></th>
			</tr>
		</thead>
		<tbody>
			<%
				Iterator<Entry<String, AbstractSet<ApiKeyValue>>> it = Controller.getInstance().getKeyController(user)
						.getKeysForUser(user).iterator();
				while (it.hasNext()) {
					Entry<String, AbstractSet<ApiKeyValue>> entry = it.next();
					for (ApiKeyValue kv : entry.getValue()) {
			%>
			<tr>
				<td scope="row"><%=entry.getKey()%></td>
				<td scope="col"><%=kv.getSort()%></td>
				<td scope="col"><%=kv.getSaveID()%></td>
				<td scope="col"><%=TextController.Get(kv.getSaveID()).getName()%></td>
				<td scope="col"><a class="fa fa-plus-square"
					onclick="editApitKey(event, '<%=entry.getKey()%>');"></a></td>
				<td scope="col"><a class="fa fa-play"
					onclick="loadTest('<%=entry.getKey()%>');"></a></td>
				<td scope="col"><a class="fa fa-trash"
					onclick="ApiKeyDelete(event)"></a></td>
			<tr>
				<%
					}
					}
				%>
			
		</tbody>
	</table>
	<br />
</div>
<div id="secoundcontent"></div>
