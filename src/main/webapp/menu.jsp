<%@page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@page import="java.util.Collection"%>
<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@page import="java.util.Locale"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null)
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<ul class="navbar-nav">
	<%
	if(user.hasRight(UserRight.backup, null, 0) || user.hasRight(UserRight.editKeys, null, 0) ||  user.hasRight(UserRight.editUser, null, 0)){%>
		<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
		href="#" id="navbardrop" data-toggle="dropdown"> <%=new I18n("config").toString(request.getLocale())%></a>
		<div class="dropdown-menu">
	<%
		if (user.hasRight(UserRight.editUser, null, 0)) {
	%><a class="dropdown-item"
		onclick="loadUserConfig()" href="#"><%=new I18n("userconfig").toString(request.getLocale())%></a>
	<%
		}
	%>
	<%
		if (user.hasRight(UserRight.editKeys, null, 0)) {
	%><a class="dropdown-item"
		onclick="loadApiConfig()" href="#"><%=new I18n("apiconfig").toString(request.getLocale())%></a>
	<%
		}
	if (user.hasRight(UserRight.backup, null, 0)) {
%><a class="dropdown-item"
	onclick="loadBackup()" href="#"><%=new I18n("Backup").toString(request.getLocale())%></a>
<%
	}%>
	</div></li>
	<%
	}
	%>
	<!-- Dropdown -->
	<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
		href="#" id="navbardrop" data-toggle="dropdown"> <%=new I18n("selectlist").toString(request.getLocale())%></a>
		<div class="dropdown-menu">
			<%
				Collection<TextController> list = user.getTextControllerList();
				for (TextController tc : user.getTextControllerList()) {
			%>
			<a class="dropdown-item" href="#" onclick='loadList(<%=tc.getID()%>)'><%=tc.getName()%></a>
			<%
				}
				if (user.hasRight(UserRight.editUser, null, 0)) {
			%>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item fa fa-plus-square" href="#"
				onclick="addTextTable()"><%=new I18n("addTextTable").toString(request.getLocale())%></a>
			<%
				}
			%>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item" href="#" onclick="loadselectlist()"><%=new I18n("overview").toString(request.getLocale())%></a>
		</div></li>
	<!-- Dropdown -->
	<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
		href="#" id="navbardrop" data-toggle="dropdown"> <%=new I18n("selectlanguage").toString(request.getLocale())%></a>
		<div class="dropdown-menu" id="LanguageSelector">
			<%
				for (Locale lc : user.getLocaleList()) {
			%>
			<a class="dropdown-item LocaleSelector LocalList fa fa-check"
				id="LocaleSelector<%=lc.getLanguage()%>"
				onclick='laguageclicked(event, "<%=lc.getLanguage()%>")'><%=lc.getDisplayLanguage(request.getLocale())%></a>
			<%
				}
				if (user.hasRight(UserRight.editUser, null, 0)) {
			%>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item fa fa-plus-square" href="#"
				onclick="addLanguage()"><%=new I18n("addlanguage").toString(request.getLocale())%></a>
			<%
				}
			%>
		</div></li>
		<li class="nav-item"><a class="nav-link"
		onclick="loadTest('')" href="#"><%=new I18n("test").toString(request.getLocale())%></a></li>
</ul>
<%
	if (list.size() == 1) {
%>
<script type="text/javascript">
$( document ).ready(function() {
	loadList(<%=list.toArray(new TextController[1])[0].getID()%>);
});
</script>
<%
	}
%>