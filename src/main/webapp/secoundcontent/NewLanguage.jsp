<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if(user == null)
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
	int SaveID = 0;
	try {
		SaveID = Integer.parseInt(request.getParameter("param"));
	} catch (Exception e) {
		SaveID = -1;
	}
	if (!user.hasRight(UserRight.editUser, null, SaveID))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("newlanguage").toString(request.getLocale())%></h2>
<%
	if (SaveID >= 0) {
%>
<p><%=new I18n("saveid").toString(request.getLocale())%>: 
	<%=TextController.Get(SaveID).getName()%>
	(<%=SaveID%>)
</p>
<form action="#"
	onsubmit="return newLanguage($('#language').val(), '<%=SaveID%>');">
	<%
		} else {
	%>
	<form action="#"
		onsubmit="return newLanguage($('#language').val(), $('#saveid').val());">
		<label for="saveid"><%=new I18n("saveid").toString(request.getLocale())%></label>
		<select id="saveid" class="form-control">
			<%
				for (TextController tc : user.getTextControllerList()) {
			%>
			<option value="<%=tc.getID()%>"><%=tc.getName()%></option>
			<%
				}
			%>
		</select>
		<%
			}
		%>
		<div class="form-group">
			<label for="language"><%=new I18n("language").toString(request.getLocale())%></label>
			<input type="text" id="language" class="form-control" />
		</div>
		<button type="submit" class="btn btn-default"><%=new I18n("add").toString(request.getLocale())%></button>
	</form>