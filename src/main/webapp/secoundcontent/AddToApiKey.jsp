<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%
	String keytoadd = request.getParameter("param");
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if(user == null|| !user.hasRight(UserRight.editKeys, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n(" ", new I18n("addTo", new I18n(" ", new I18n("apikey")))).toString(request.getLocale())%></h2>
<p><%=new I18n("apikey").toString(request.getLocale())%>:
	<%=keytoadd%></p>
<form action="#" onsubmit="return addToApiKey('<%=keytoadd%>',$('#saveid').val(),$('#sort').val());">
	<div class="form-group">
		<label for="saveid"><%=new I18n("saveid").toString(request.getLocale())%></label>
		<select id="saveid" class="form-control">
			<%
				for (TextController tc : user.getTextControllerList()) {
			%>
			<option value="<%=tc.getID()%>"><%=tc.getName()%></option>
			<%
				}
			%>
		</select>
	</div>
	<div class="form-group">
		<label for="sort"><%=new I18n("wishsort").toString(request.getLocale())%></label>
		<input type="number" min="0" value="0" class="form-control" id="sort">
	</div>
	<button type="reset" class="btn btn-default"
		onclick="LoadSecoundContent('NewApiKey'); resetLastEditEditButton();" value="Reset"><%=new I18n("cancel").toString(request.getLocale())%></button>
	<button type="submit" class="btn btn-default"><%=new I18n("add").toString(request.getLocale())%></button>
</form>