<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.service.Model.UserRight"%>

<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);

if(user == null|| !user.hasRight(UserRight.editKeys, null, 0))
	Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("newapikey").toString(request.getLocale()) %></h2>
<form action="#" onsubmit="return newApiKey($('#saveid').val());">
	<div class="form-group">
		<label for="saveid"><%=new I18n("saveid").toString(request.getLocale()) %></label>
		<select id="saveid" class="form-control">
			<%
				for (TextController tc : user.getTextControllerList()) {
			%>
			<option value="<%=tc.getID()%>"><%=tc.getName()%></option>
			<%
				}
			%>
		</select>
	</div>
	<button type="submit" class="btn btn-default"><%=new I18n("add").toString(request.getLocale()) %></button>
</form>