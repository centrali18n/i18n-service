<%@page import="net.spottog.i18n.service.Model.UserRight"%>
<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null || !user.hasRight(UserRight.editUser, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("addTextTable").toString(request.getLocale())%></h2>
<form action="#"
	onsubmit="return newTextTable($('#textTableName').val(), $('#DefaultExpire').val());">
	<div class="form-group">
		<label for="textTableName"><%=new I18n("name").toString(request.getLocale())%></label>
		<input type="text" id="textTableName" class="form-control" />
		<label for="DefaultExpire"><%=new I18n("DefaultExpire").toString(request.getLocale())%></label>
		<input type="number" min="0" id="DefaultExpire" class="form-control" />
	</div>
	<button type="submit" class="btn btn-default"><%=new I18n("add").toString(request.getLocale())%></button>
</form>