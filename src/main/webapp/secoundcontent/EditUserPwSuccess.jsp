<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.service.Model.UserRight"%>

<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null || !user.hasRight(UserRight.editUser, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
	net.spottog.i18n.service.Model.User userToEdit = Controller.getInstance().getUserController().getUserFromId(Integer.parseInt(request.getParameter("param")));
%>
<div id="backtoOverview">
	<a onclick="loadUserConfig()"><%=new I18n("back to overview").toString(request.getLocale())%></a>
</div>
<h2><%=new I18n("EditUserPWSuccess").toString(request.getLocale()) + ": (" + userToEdit.getID() + ") " + userToEdit.getEmail()%></h2>