<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.service.Model.UserRight"%>

<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);

	if (user == null || !user.hasRight(UserRight.editUser, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
%>
<h2><%=new I18n("newUser").toString(request.getLocale())%></h2>
<form action="#" onsubmit="addNewUser($('#email').val(), $('#pwd1').val(), $('#pwd2').val()); return false;">
	<div class="form-group">
		<label for="email"><%=new I18n("Email").toString(request.getLocale())%></label>
		<input type="email" id="email" class="form-control" />
	</div><div class="form-group">
		<label for="pwd1"><%=new I18n("password").toString(request.getLocale())%></label>
		<input type="password" class="form-control" title="<%=new I18n("pwdescription").toString(request.getLocale())%>" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" id="pwd1" name="pwd1" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.pwd2.pattern = RegExp.escape(this.value);" />
	</div><div class="form-group">
		<label for="pwd2"><%=new I18n("passwordconfirm").toString(request.getLocale())%></label>
		<input type="password"  class="form-control"  title="<%=new I18n("pwconfirmdescription").toString(request.getLocale())%>"  id="pwd2" name="pwd2" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" />
	</div>
	<button type="reset" class="btn btn-default" onclick="loadUserConfig()"
		value="Reset"><%=new I18n("cancel").toString(request.getLocale())%></button>
	<button type="submit" class="btn btn-default"><%=new I18n("add").toString(request.getLocale())%></button>
</form>