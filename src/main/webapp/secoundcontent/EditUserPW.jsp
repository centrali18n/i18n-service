<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.service.Model.UserRight"%>

<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null || !user.hasRight(UserRight.editUser, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
	net.spottog.i18n.service.Model.User userToEdit = Controller.getInstance().getUserController().getUserFromId(Integer.parseInt(request.getParameter("param")));
%>
<div id="backtoOverview">
	<a onclick="loadUserConfig()"><%=new I18n("back to overview").toString(request.getLocale())%></a>
</div>
<h2><%=new I18n("EditUserPW").toString(request.getLocale()) + ": (" + userToEdit.getID() + ") " + userToEdit.getEmail()%></h2>
<form action="#" onsubmit="SetUserPW('<%=userToEdit.getID() %>', $('#pwd1').val(), $('#pwd2').val()); return false;">
	</div><div class="form-group">
		<label for="pwd1"><%=new I18n("password").toString(request.getLocale())%></label>
		<input type="password" class="form-control" title="<%=new I18n("pwdescription").toString(request.getLocale())%>" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" id="pwd1" name="pwd1" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.pwd2.pattern = RegExp.escape(this.value);" />
	</div><div class="form-group">
		<label for="pwd2"><%=new I18n("passwordconfirm").toString(request.getLocale())%></label>
		<input type="password"  class="form-control"  title="<%=new I18n("pwconfirmdescription").toString(request.getLocale())%>"  id="pwd2" name="pwd2" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" />
	</div>
	<button type="reset" class="btn btn-default" onclick="loadUserConfig()"
		value="Reset"><%=new I18n("cancel").toString(request.getLocale())%></button>
	<button type="submit" class="btn btn-default"><%=new I18n("set").toString(request.getLocale())%></button>
</form>