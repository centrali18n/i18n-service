<%@ page import="net.spottog.i18n.global.I18n"%>
<%@ page import="net.spottog.i18n.service.Controller.Controller"%>
<%@ page import="net.spottog.i18n.service.Controller.UserController"%>
<%@ page import="net.spottog.i18n.service.Controller.TextController"%>
<%@ page import="net.spottog.i18n.service.Model.User"%>
<%@ page import="net.spottog.i18n.service.Model.CockieObjects"%>
<%@ page import="net.spottog.i18n.service.Model.UserRight"%>
<%@ page import="net.spottog.i18n.service.Model.LocaleWithWriteAccess"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.Collection"%>
<style>
.slow .toggle-group {
	transition: left 0.7s;
	-webkit-transition: left 0.7s;
}
</style>
<%
	net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(request);
	if (user == null || !user.hasRight(UserRight.editUser, null, 0))
		Controller.getInstance().IllegalUnauthorizedAccessError(null);
	net.spottog.i18n.service.Model.User userToEdit = Controller.getInstance().getUserController()
	.getUserFromId(Integer.parseInt(request.getParameter("param")));
	boolean editMe = user == userToEdit;
	Collection<TextController> TextRightList = userToEdit.getTextControllerList();
	LinkedList<TextController> canAdd = new LinkedList<>();
	for(TextController tc : TextController.getList()){
		if(!TextRightList.contains(tc))
	canAdd.add(tc);
	}
	int writebuttonctr = 0;
%>
<div class="row">
	<div class="card-body">
		<a onclick="loadUserConfig()"><%=new I18n("back to overview").toString(request.getLocale())%></a>
		<h2><%=new I18n("EditUser").toString(request.getLocale()) + ": (" + userToEdit.getID() + ") "
					+ userToEdit.getEmail()%></h2>
		<%
			if (editMe) {
		%>
		<div class="alert alert-danger alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><%=new I18n("warning").toString(request.getLocale())%></strong>
			<%=new I18n("you edit your self").toString(request.getLocale())%>
		</div>
		<%
			}
			if(!canAdd.isEmpty()){
		%>
		<form action="#" onsubmit="addRightToUser('<%=userToEdit.getID()%>', $('#saveid').val(), 'false');">
			<div class="form-group">
				<label for="saveid"><%=new I18n("saveid").toString(request.getLocale()) %></label>
				<select id="saveid" class="form-control">
					<%
				for (TextController tc : canAdd) {
			%>
					<option value="<%=tc.getID()%>"><%=tc.getName()%></option>
					<%
				}
			%>
				</select>
			</div>
			<button type="submit" class="btn btn-default"><%=new I18n("add").toString(request.getLocale()) %></button>
		</form>
		<%} %>
	</div>
		<div class="col-sm-4" align="center">
			<p>
				<label for="alloweditUser"><%=new I18n("alloweditUser").toString(request.getLocale())%></label>
			</p>
			<p>
				<input id="alloweditUser" type="checkbox"
					<%=userToEdit.hasRight(UserRight.editUser, null, 0) ? " checked" : ""%>
					data-toggle="toggle" data-width="100" data-style="slow"
					data-onstyle="outline-dark" data-offstyle="outline-secondary">
			</p>
			<p>
				<label for="alloweditApi"><%=new I18n("alloweditApi").toString(request.getLocale())%></label>
			</p>
			<p>
				<input id="alloweditApi" type="checkbox"
					<%=userToEdit.hasRight(UserRight.editKeys, null, 0) ? " checked" : ""%>
					data-toggle="toggle" data-width="100" data-style="slow"
					data-onstyle="outline-dark" data-offstyle="outline-secondary">
			</p>
			<p>
				<label for="allowbackup"><%=new I18n("allowbackup").toString(request.getLocale())%></label>
			</p>
			<p>
				<input id="allowbackup" type="checkbox"
					<%=userToEdit.hasRight(UserRight.backup, null, 0) ? " checked" : ""%>
					data-toggle="toggle" data-width="100" data-style="slow"
					data-onstyle="outline-dark" data-offstyle="outline-secondary">
			</p>
		</div>
</div>
<div class="row">

	<%
		final String WriteText = "<i class='fa fa-pencil'></i> " + new I18n("write").toString(request.getLocale());
		final String ReadText = "<i class='fa fa-search'></i> " + new I18n("read").toString(request.getLocale());
		for (TextController tc : TextRightList) {
	%>
	<div class="col-sm-4 card" style="width: 18rem;">
		<div class="card-body">
			<div class="row">
				<div class="col-sm-10">
					<h5 class="card-title"><%=tc.getName()%></h5>
				</div>
				<div class="col-sm-2">
					<a href="#" onclick="removeUserRightForTextSave('<%=userToEdit.getID() %>', '<%=tc.getID()%>');" class="text-secondary"><i
						class='fa fa-trash'></i></a>
				</div>
			</div>
			<h6 class="card-subtitle mb-2 text-muted"><%=new I18n("saveid").toString(request.getLocale())%>:
				<%=tc.getID()%></h6>
			<%
				for (LocaleWithWriteAccess locale : userToEdit.getLocaleList(tc.getID())) {
						final String writebuttonid = "haswriteacessbtn" + writebuttonctr++;
			%>
			<div class="row">
				<%
					if (null == locale.getLanguage()) {
				%>
				<div class="col-sm-8">
					<select class="form-control"
						onchange="userRightTextSaveAllLangChange('<%=userToEdit.getID() %>', '<%=tc.getID() %>', $(this).val(), $('#<%=writebuttonid %>').prop('checked'))">
						<option value="0"><%=new I18n("allLanguages").toString(request.getLocale())%></option>
						<%
							for (Locale l : userToEdit.getLocaleList()) {
						%>
						<option value="<%=l.toString()%>"><%=l.getDisplayLanguage(request.getLocale())%></option>
						<%
							}
						%>
					</select>
				</div>
				<%
						} else {
					%>

				<div class="col-sm-6">
					<%=locale.getLanguage().getDisplayLanguage(request.getLocale())%>

				</div>
				<div class="col-sm-2">
					<a href="#" onclick="removeUserRightForTextSave('<%=userToEdit.getID() %>', '<%=tc.getID() %>', '<%=locale.getLanguage().getLanguage() %>')" class="text-secondary"><i
						class='fa fa-trash'></i></a>
				</div>
				<%
					}%>
				<div class="col-sm-4">
					<input id="<%=writebuttonid%>" type="checkbox"
						<%=locale.isWriteAccess() ? " checked" : ""%> data-toggle="toggle"
						data-style="slow" data-onstyle="outline-dark"
						data-offstyle="outline-secondary" data-on="<%=WriteText%>"
						data-off="<%=ReadText%>">
				</div>

			</div>
			<%
				if (null != locale.getLanguage()) {
			%>

			<a href="#" class="text-secondary card-link" onclick="addRightToUser('<%=userToEdit.getID() %>', '<%=tc.getID() %>'); return false;"><%=new I18n("add").toString(request.getLocale())%></a>
			<%
				}
			%>
			<script type="text/javascript">
				$('#<%=writebuttonid %>').bootstrapToggle();
				$('#<%=writebuttonid %>').change(function() { 
					userRightChangeTextWrite('<%=userToEdit.getID()%>', $(this).prop('checked') ,'<%=tc.getID() %>' ,'<%=null == locale.getLanguage() ? "" : locale.getLanguage().getLanguage() %>');
				});
			</script>
				<%
				}
			%>
			</div>
		</div>
		<%
		}
	%>
	</div>
	<script type="text/javascript">
	$('#alloweditUser').bootstrapToggle();
	$('#alloweditUser').change(function() { 
		userRightChange('<%=userToEdit.getID()%>', $(this).prop('checked'), 'editUser');
		<%if (editMe) {%>
		$("#navbarText").load("menu.jsp");
		loadselectlist();
		<%}%>
		});
	$('#alloweditApi').bootstrapToggle();
	$('#alloweditApi').change(function() {
		userRightChange('<%=userToEdit.getID()%>', $(this).prop('checked'), 'editKeys');
<%if (editMe) {%>
	$("#navbarText").load("menu.jsp");
<%}%>
	});
	$('#allowbackup').bootstrapToggle();
	$('#allowbackup').change(function() {
		userRightChange('<%=userToEdit.getID()%>', $(this).prop('checked'), 'backup');
<%if (editMe) {%>
	$("#navbarText").load("menu.jsp");
<%}%>
	});
</script>