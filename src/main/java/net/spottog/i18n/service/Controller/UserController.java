package net.spottog.i18n.service.Controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import net.spottog.i18n.global.I18n;
import net.spottog.i18n.service.Model.DatabaseConnection;
import net.spottog.i18n.service.Model.User;
import net.spottog.i18n.service.Model.UserRight;
import net.spottog.i18n.service.Model.Transport.EditText.UnsupportedStateException;
/**
 * Im UserController werden die im i18n Service vorhandenen User verwaltet.
 */
public class UserController {
	private final Controller controller;
	/**
	 * Ermöglicht die Schnelle Zuordnung ID -> User
	 */
	private final ConcurrentHashMap<Integer, User> IDSave = new ConcurrentHashMap<Integer, User>();
	/**
	 * Ermöglicht die Schnelle Zuordnung email -> User
	 */
	private final ConcurrentHashMap<String, User> EmailSave = new ConcurrentHashMap<String, User>();
	/**
	 * Initialisiert den User Controller.
	 * Dabei werden die werte aus der Datenbank in den Speicher geladen.
	 * @param controller der Controller für den Zugriff auf die anderen Controller.
	 */
	public UserController(Controller controller) {
		this.controller = controller;
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final Statement st = con.createStatement();
				final ResultSet rs = st.executeQuery(
						"SELECT \"User\".\"ID\" AS ID, \"User\".\"email\" AS email, \"User\".\"pwhash\" AS pwhash, \"User\".\"edituser\" AS edituser, \"User\".\"editkeys\" AS editkeys, \"User\".\"backup\" AS backup, \"userright\".\"language\" AS language, \"userright\".\"write\" AS textwrite, \"userright\".\"SaveID\" AS SaveID  FROM \"User\" FULL OUTER JOIN \"userright\" ON \"ID\" = userright.user");) {
			while (rs.next()) {
				User user = AddUserLocal(rs.getInt("ID"), rs.getString("email"), rs.getString("pwhash"));
				String LocaleString = rs.getString("language");
				user.addRightLocal(UserRight.editUser, rs.getBoolean("edituser"), LocaleString, 0);
				user.addRightLocal(UserRight.editKeys, rs.getBoolean("editkeys"), LocaleString, 0);
				user.addRightLocal(UserRight.backup, rs.getBoolean("backup"), LocaleString, 0);
				if (null != LocaleString) {	//new user has no rights
					user.addRightLocal(UserRight.writeText, rs.getBoolean("textwrite"), LocaleString, rs.getInt("SaveID"));
				}
			}
		} catch (SQLException e) {
			System.out.println("Error im TextCotroller: " + e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * Erzeugt ein User Objekt, und fügt es dem lokalen Speicher hinzu.
	 * @param ID DIe eindeutige user id
	 * @param email die email Adresse des users.
	 * @param pwhash der Passwort hash in dem vom SecureController gewünschtem Format.
	 * @return das User Objekt.
	 */
	private User AddUserLocal(final Integer ID, final String email, final String pwhash) {
		final User user = IDSave.computeIfAbsent(ID, new Function<Integer, User>() {
			public User apply(final Integer ID) {
				return new User(ID, email, pwhash);
			}
		});
		EmailSave.computeIfAbsent(email.toLowerCase(), new Function<String, User>() {
			public User apply(final String email) {
				return user;
			}
		});
		return user;
	}
	/**
	 * Löscht einen Benutzer incl. aller rechte Aus dem Lokalem Speicher und der Datenbank
	 * Es ist nicht möglich den letzten Benutzer, welcher die berechtigung hat Benutzer zu bearbeiten zu Löschen.
	 * @param userid die eindeutige id des Benutzer welcher gelöscht werden soll.
	 * @throws SQLException  Fehler in der Datenbank.
	 * @throws UnsupportedStateException Wenn versucht wird den Letzten Benutzer mit Dem Recht benutzer anzulegen zu löschen.
	 */
	public void DelUser(Integer userid) throws SQLException {
		User userToDelete = IDSave.get(userid);
		if(!userToDelete.hasRight(UserRight.editUser, null, 0) || countUserWithEditUserRights() > 1) {	//Not delete the last user.
			final String sql1 = "Delete From \"userright\" WHERE \"user\"=?";
			final String sql2 = "Delete From \"User\" WHERE \"ID\"=?";
			try (final DatabaseConnection con = DatabaseConnection.Get();
					final PreparedStatement st1 = con.prepareStatement(sql1);
					final PreparedStatement st2 = con.prepareStatement(sql2);) {
				st1.setInt(1, userid);
				st1.executeUpdate();
				st2.setInt(1, userid);
				st2.executeUpdate();
				EmailSave.remove(userToDelete.getEmail());
				IDSave.remove(userid);
			}
		}else {
			throw new UnsupportedStateException(new I18n("no Delete Last User Error"));
		}
	}
	/**
	 * Anzahl der Benutzer mit dem Recht Benutzer zu Bearbeiten.
	 * @return Anzahl der Benutzer.
	 */
	public long countUserWithEditUserRights() {
		return IDSave.values().stream().filter(user -> user.hasRight(UserRight.editUser, null, 0)).count();
	}
	/**
	 * Erzeugt einen neuen Benutzer
	 * @param email die email Adresse
	 * @param passwd Das Passwort im Klartext
	 * @return des Benutzer Objekt
	 */
	public User addUser(final String email, final String passwd) {
		final String sql = "INSERT INTO \"User\" (\"email\", \"pwhash\") VALUES (?, ?)";
		final String pwhash = controller.getSecure().generatePW(passwd);
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			int index = 0;
			st.setString(++index, email);
			st.setString(++index, pwhash);
			st.executeUpdate();
			st.getGeneratedKeys().next(); // einmal Aufrufen.
			final int id = st.getGeneratedKeys().getInt(1);
			AddUserLocal(id, email, pwhash);
		} catch (SQLException e) {
			System.out.println("Error beim Anlegen des Benutzers: " + e.getMessage());
			e.printStackTrace();
		}
		return LoginUser(email, passwd);
	}
	/**
	 * Vergibt einem User ein neues Passwort.
	 * @param user der Benutzer
	 * @param passwd das klartextpasswort.
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public void setUserPasswd(final User user, final String passwd) throws SQLException {
		final String sql = "Update \"User\" Set \"pwhash\"=? Where \"ID\"=?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			st.setString(1, user.ChangePasswd(passwd));
			st.setInt(2, user.getID());
			st.executeUpdate();
		}
	}
	/**
	 * der Benutzer wird eingelogt und das Benutzerobjekt zurückgegeben.
	 * @param email die email Addresse.
	 * @param passwd das Passwort im klartext
	 * @return das benutzer element.
	 */
	public User LoginUser(final String email, final String passwd) {
		User user = EmailSave.get(email.toLowerCase());
		if (null == user || !user.CheckPasswd(passwd))
			Controller.getInstance()
					.IllegalUnauthorizedAccessError(new I18n("I can not do anything with these access data."));
		return user;
	}

	/**
	 * Löscht ein Text speicher in allen Benutzerrechten von allen Benutzern Dauerhaft und mit sofortiger wirkung.
	 * @param SaveID die id des Textspeichers.
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public void RemoveTextSave(int SaveID) throws SQLException {
		final String sql = "DELETE FROM \"userright\" WHERE \"SaveID\"=?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			st.setInt(1, SaveID);
			st.executeUpdate();
			for (User user : IDSave.values()) {
				user.delAllRightLocal(SaveID);
			}
		}
	}
	/**
	 * gibt ein User element anhand seiner id zurück.
	 * @param id die eindeutige id
	 * @return das Benutzer element.
	 */
	public User getUserFromId(Integer id) {
		return IDSave.get(id);
	}
	/**
	 * Eine Liste von allen Benutzern
	 * @return die Auflistung.
	 */
	public Collection<User> getList() {
		return IDSave.values();
	}
}
