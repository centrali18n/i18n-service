package net.spottog.i18n.service.Controller;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
/**
 * Sorgt dafür, das beim laden vom Tomcat Alle Controller und Singletons initialisiert werden.
 * beim beenden wird im gegensatz dazu alle Datenbank verbindungen geschlossen.
 */
public class ControllerStartupManager implements ServletContextListener {
	/**
	 * Das Singleton beim Abrufen, damit dieses Erzeugt wird.
	 */
	public static final Controller controller = Controller.getInstance();
	/**
	 * Start der Anwendung
	 */
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("i18n.spottog.net Started");
	}
	/**
	 * Beenden der Anwendung
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		try {
			controller.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
