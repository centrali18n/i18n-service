package net.spottog.i18n.service.Controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.Map;

import net.spottog.i18n.global.I18n;
import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;
import net.spottog.i18n.service.Model.DatabaseConnection;
import net.spottog.i18n.service.Model.ExpireText;
import net.spottog.i18n.service.Model.ITextResponder;
import net.spottog.i18n.service.Model.User;
import net.spottog.i18n.service.Model.UserRight;
import net.spottog.i18n.service.Model.Transport.EditText.UnsupportedStateException;
/**
 * Text Controller für einen Text Speicher.
 * Um An Einen Text Speicher zu kommen sollte die static Get methode verwendet werden.
 * Beim Ersten Zugriff werden die Texte in den Speicher gelesen und dort gehalten. 
 */
public class TextController implements ITextResponder {
	private final ConcurrentHashMap<String, ConcurrentHashMap<String, ExpireText>> localeSave = new ConcurrentHashMap<String, ConcurrentHashMap<String, ExpireText>>();
	private static final ConcurrentHashMap<Integer, TextController> ControllerSave = new ConcurrentHashMap<Integer, TextController>();
	private static Controller controller = Controller.getInstance();
	protected final int SaveID;
	private String Name;
	private int DefaultExpire;
	/**
	 * Gibt den Text Save Zurück
	 * Der Erste Zugriff erfolgt aus der Datenbank die Anschließenden aus dem Speicher.
	 * @param id die ID
	 * @return der Text Speicher.
	 */
	public static TextController Get(final Integer id) {
		return ControllerSave.computeIfAbsent(id, new Function<Integer, TextController>() {
			public TextController apply(final Integer id) {
				return new TextController(id);
			}
		});
	}
	/**
	 * Ließt alle Elemente aus der Datenbank und hält diese im Speicher
	 * @param SaveID die Eindeutige id.
	 */
	private TextController(final int SaveID) {
		this.SaveID = SaveID;
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final Statement st = con.createStatement();
				final ResultSet rs = st.executeQuery(
						"SELECT \"Name\", \"DefaultExpire\" FROM \"TextName\" WHERE \"SaveID\" = " + SaveID);) {
				rs.next();
				this.Name = rs.getString("Name");
				this.DefaultExpire = rs.getInt("DefaultExpire");
		} catch (SQLException e) {
			this.Name = String.valueOf(SaveID);
			System.out.println("Error im TextCotroller: " + e.getMessage());
			e.printStackTrace();
		}
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final Statement st = con.createStatement();
				final ResultSet rs = st.executeQuery(
						"SELECT \"SearchString\", \"Language\", \"Text\", \"timespan\" FROM \"Texte\" WHERE \"SaveID\" = "
								+ SaveID);) {
			while (rs.next()) {
				final String SearchString = rs.getString("SearchString");
				final Locale Language = StringToLocale(rs.getString("Language"));
				final String Text = rs.getString("Text");
				final int timespan = rs.getInt("timespan");
				ExpireText expiretext = new ExpireText(Text, timespan);
				AddOrReplaceLocale(Language, SearchString, expiretext);
			}
		} catch (SQLException e) {
			System.out.println("Error im TextCotroller: " + e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * Eine Liste aller Speicher, welche sich schon im Speicher befinden.
	 * @return die Liste.
	 */
	public static Collection<TextController> getList(){
		return ControllerSave.values();
	}
	/**
	 * Erzeugt einen Neuen Text Speicher
	 * @param name den für menschen lesbaren namen
	 * @param user der anlegende User damit er gleich entsprechende rechte bekommt.
	 * @param DefaultExpire Ablaufzeit für neue Texte
	 * @return die id des erzteugten text Speichers
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public static int CreateNew(final String name, final User user, final int DefaultExpire) throws SQLException {
		final String sql = "INSERT INTO \"TextName\" (\"Name\", \"DefaultExpire\") VALUES (?, ?)";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			st.setString(1, name);
			st.setInt(2, DefaultExpire);
			st.executeUpdate();
			st.getGeneratedKeys().next(); // einmal Aufrufen.
			final int id = st.getGeneratedKeys().getInt(1);
			user.CreateNewRight(UserRight.writeText, null, id);
			return id;
		}
	}
	/**
	 * Löscht einen Text Speicher.
	 * @param textController den Entsprehcenden Text Controller
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public static void Delete(TextController textController) throws SQLException {
		if(!textController.isEmpty())
			throw new UnsupportedStateException(new I18n("textsave", new I18n(" ", new I18n(textController.getName(), new I18n(" is not empty")))));
		controller.getUserController().RemoveTextSave(textController.SaveID);
		controller.getKeyController().RemoveTextSave(textController.SaveID);
		final String sql = "DELETE FROM \"TextName\" WHERE \"SaveID\"=?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			st.setInt(1, textController.SaveID);
			st.executeUpdate();
			ControllerSave.remove((Object)controller);
		}
	}
	/**
	 * Hilfsklasse um eine Sprache zu parsen
	 * @param Language Die Sprache als String
	 * @return die Sprache als locale
	 */
	public static Locale StringToLocale(String Language){
		if(Language == null) Language = " ";
		String[] Lang = Language.split("_");
		if(Lang.length == 2) {
		return  new Locale(Lang[0], Lang[1]);
		}
		return new Locale(Language);
	}
	/**
	 * Löscht einen Einzelnen Text aus dem Speicher.
	 * @param SearchString der index.
	 */
	public void Delete(final String SearchString) {
		String sql = "DELETE FROM \"Texte\" WHERE \"SaveID\" = ? AND \"SearchString\" = ?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setInt(++index, SaveID);
			st.setString(++index, SearchString);
			st.executeUpdate();
			for (ConcurrentHashMap<String, ExpireText> locale : localeSave.values()) {
				DeleteFromLocale(locale, SearchString);
			}
		} catch (SQLException e) {
			System.out.println("Error beim Löschen: " + e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * Löscht einen Text in einer Sprache.
	 * @param Language Die Sprache
	 * @param SearchString der Index des Textes.
	 */
	public void Delete(final Locale Language, final String SearchString) {
		String sql = "DELETE FROM \"Texte\" WHERE \"SaveID\" = ? AND \"Language\" = ? AND \"SearchString\" = ?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setInt(++index, SaveID);
			st.setString(++index, Language.toString());
			st.setString(++index, SearchString);
			st.executeUpdate();
			DeleteFromLocale(localeSave.getOrDefault(Language.getLanguage(), null), SearchString);
		} catch (SQLException e) {
			System.out.println("Error beim Löschen: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void DeleteFromLocale(final ConcurrentHashMap<String, ExpireText> locale, final String SearchString) {
		if (locale == null)
			return;
		locale.remove(SearchString);
		if(locale.isEmpty())
			localeSave.remove((Object)locale);
	}
	/**
	 * Fügt dem Text Speicher einen neuen Text Hinzu.
	 * ist ein Entsprechender Text schon vorhanden, wird er ersetzt.
	 * @param Language die Sprache
	 * @param SearchString der Text Index
	 * @param Text der übersetzte Text
	 * @param timespan die Zeit, wann der der Text abläuft.
	 */
	public void AddOrReplace(final Locale Language, final String SearchString, final String Text, final int timespan) {
		String sql = "INSERT INTO \"Texte\" (\"SaveID\", \"SearchString\", \"Language\", \"Text\", \"timespan\") VALUES (?, ?, ?, ?, ?)  ON CONFLICT (\"SaveID\", \"SearchString\", \"Language\") DO UPDATE SET \"Text\" = ?, \"timespan\" = ?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setInt(++index, SaveID);
			st.setString(++index, SearchString);
			st.setString(++index, Language.toString());
			st.setString(++index, Text);
			st.setInt(++index, timespan);
			st.setString(++index, Text);
			st.setInt(++index, timespan);
			st.executeUpdate();
			AddOrReplaceLocale(Language, SearchString, new ExpireText(Text, timespan));
		} catch (SQLException e) {
			System.out.println("Error beim TextUpdate: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void AddOrReplaceLocale(final Locale Language, final String SearchString, final ExpireText text) {
		final ConcurrentHashMap<String, ExpireText> language = localeSave.computeIfAbsent(Language.getLanguage(),
				new Function<String, ConcurrentHashMap<String, ExpireText>>() {
					public ConcurrentHashMap<String, ExpireText> apply(String key) {
						return new ConcurrentHashMap<String, ExpireText>();
					}
				});
		language.put(SearchString, text);
	}

	@Override
	public ITextWithLanguageAnswer search(final TextWithLanguageRequest request) {
		final ExpireText text = getExpireText(request.getLanguage().getLanguage(), request.getSearchString());
		if(null == text)
			return null;
		return text.createAnswer(request);
	}
	/**
	 * Sucht einen Text und gibt in als Ablaufenden Text Zurück.
	 * @param language die Sprache
	 * @param SearchString den zu suchenden Text
	 * @return das Text Element
	 */
	public ExpireText getExpireText(final String language, final String SearchString) {
		final ConcurrentHashMap<String, ExpireText> locale = localeSave.getOrDefault(language, null);
		if (null == locale)
			return null;
		return locale.getOrDefault(SearchString, null);
	}
	/**
	 * Die Eindeutige id des Text Speichers.
	 * @return Eindeutige id
	 */
	public int getID() {
		return SaveID;
	}
	/**
	 * Ein menschen lesbarer name für den Text Speicher
	 * @return name
	 */
	public String getName() {
		return Name;
	}
	/**
	 * Gibt eine Liste mit allen Sprachen, für welche der text speicher elemente beinhaltet zurück
	 * @return alle Sprachen
	 */
	public Collection<Locale> getLanguageList() {
		List<Locale> ret = new LinkedList<>();
		Enumeration<String> languages = localeSave.keys();
		while(languages.hasMoreElements())
			ret.add(TextController.StringToLocale(languages.nextElement()));
		return ret;
	}
	/**
	 * Erzeugt eine Liste mit den Texten für eine Sprache.
	 * @param languages liste der Sprache, welche abgerufen werden.
	 * @return die Liste
	 */
	public Map<String, Map<String, ExpireText>> get3DMapFor(String[] languages){
		Map<String, Map<String, ExpireText>> map = new Hashtable<>();
		for (String lang : languages) {
			ConcurrentHashMap<String, ExpireText> langmap = localeSave.getOrDefault(lang, null);
			if(langmap!= null) {
				for (Map.Entry<String, ExpireText> ie : langmap.entrySet()) {
					Map<String, ExpireText> m = map.getOrDefault(ie.getKey(), null);
					if(m == null) {
						m = new Hashtable<>();
						map.put(ie.getKey(), m);
					}
					m.put(lang, ie.getValue());
				}
			}
		}
		return map;
	}
	/**
	 * Prüft ob ein Text Speicher leer ist
	 * @return true wenn leer.
	 */
	public boolean isEmpty() {
		return localeSave.isEmpty();
	}
	/**
	 * voreinstellung für das ablaufen der texte.
	 * @return die Standard zeit.
	 */
	public int getDefaultExpireTime() {
		return DefaultExpire;
	}
}
