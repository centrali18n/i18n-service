package net.spottog.i18n.service.Controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import net.spottog.i18n.global.ITextSave;
import net.spottog.i18n.global.ITextWithLanguage;
import net.spottog.i18n.service.Model.ExpireText;
/**
 * Ein Text Speicher, welche dem internen I18n System zur verfügung steht.
 * Als Backend wird direkt der Text Speicher mit der id 0 genutzt.
 * Dies erlaubt dem i18n Service seine Eigene Texte zu pflegen.
 */
public class LocalTextSave extends ITextSave {
	/**
	 * Ein Text Speicher, welche dem internen I18n System zur verfügung steht.
	 * Als Backend wird direkt der Text Speicher mit der id 0 genutzt.
	 * Dies erlaubt dem i18n Service seine Eigene Texte zu pflegen.
	 * @param next der Nächste Text in der Verkettung.
	 */
	public LocalTextSave(ITextSave next) {
		super(next);
	}

	@Override
	public ITextWithLanguage GetLocal(String SearchString, Locale language) {
		ExpireText text = TextController.Get(0).getExpireText(language.getLanguage(), SearchString);
		if (text != null)
			return new ITextWithLanguage() {

				@Override
				public String getSearchString() {
					return SearchString;
				}

				@Override
				public Locale getLanguage() {
					return language;
				}

				@Override
				public String getText() {
					return text.getText();
				}

				@Override
				public Date getExpire() {
					if (text.getExpireTime() > 0) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.HOUR, text.getExpireTime());
						return cal.getTime();
					}
					// Wenn kleiner 0 kein ablauf
					return new Date(Long.MAX_VALUE);
				}

				@Override
				public boolean isValid() {
					return true;
				}
			};
		return null;
	}

}
