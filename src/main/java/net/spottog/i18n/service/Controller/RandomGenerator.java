package net.spottog.i18n.service.Controller;

import java.security.SecureRandom;
/**
 * der Randomgenerator erzeugt zufallszahlen für die Sicherheitsrelevanten Funktionen.
 */
public class RandomGenerator {
	private final SecureRandom random = new SecureRandom();
	protected final static int APIKEYLENGTHLENGTH = 64;
	private final static int SALTBYTESCOUNT = 48;
	private final Controller controller;
	/**
	 * Initialisiert den Randomgenerator.
	 * @param controller Zugriff auf die Controller.
	 */
	public RandomGenerator(final Controller controller) {
		this.controller = controller;
	}
	/**
	 * Erzeugt einen neuen Apikey welches ein zufallstring mit fester länge ist.
	 * @return zufallstring mit fester länge
	 */
	public String getNextApiKey() {
		return getRandomString(APIKEYLENGTHLENGTH);
	}
	/**
	 * Erzeugt einen Zufallsstring in der angegeben länge.
	 * @param length die länge muss eine vielzahl von 8 sein.
	 * @return zufallstring mit der gewünschten länge
	 */
	public String getRandomString(final int length) {//Länge der Bytes Base64
		return controller.getSecure().Encode(getBytes((length / 8) * 6));	
	}
	/**
	 * Erzeugt eine Zufallszahl, in dem Gewünschten intervall
	 * @param miniterations Kleinst mögliche Zuffallszahl
	 * @param maxiterations größtmögliche Zufallszahl
	 * @return die Zufallszahl.
	 */
	public int getInt(final int miniterations,final int maxiterations) {
		if(maxiterations < 0 ||miniterations > maxiterations)
			throw new IllegalAccessError("illegal arguments for new random int");
		return miniterations + random.nextInt(maxiterations - miniterations);
	}
	/**
	 * Erzeugt eine Zufallszahl, um das Benutzerpasswort sicher in der Datenbank zu Speichern.
	 * @return zufällige bytes.
	 */
	public byte[] getSalt() {
		return getBytes(SALTBYTESCOUNT);
	}
	/**
	 * Erzeugt die Angegebene Länge an Zufälligen daten.
	 * @param length anzahl der bytes
	 * @return das array mit zufallsdaten
	 */
	public byte[] getBytes(final int length) {
		final byte[] barray = new byte[length]; 
		random.nextBytes(barray);
		return barray;
	}
	
}
