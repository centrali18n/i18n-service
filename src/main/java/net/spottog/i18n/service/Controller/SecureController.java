package net.spottog.i18n.service.Controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
/**
 * Stellt Kryptografische Funktionen für die Benutzerverwaltung, Passwortschult, Apikey Generation u.v.m. Bereit.
 */
public class SecureController {
	private static final String PEPPER = "net.spottog.i18n.Password Pepper.";
	private final static int PWKEYLENGTH = 64 * 6;
	private final static int MINITERATIONS = 900;
	private final static int MAXITERATIONS = 1000;
	private final RandomGenerator random;
	private SecretKeyFactory securekeyfactory;
	/**
	 * Die Anzahl der Passwort hashes verändert sich bei jedem Neustart, so das sich auf dauer untschiedliche passwörter in der datenbank befinden.
	 * der Wert kann über die Konfigurationstabelle beinflusst werden, wenn im Laufe der Zeit die Hardware Schneller wird.
	 */
	public final int iterations;
	private final Encoder base64enc = Base64.getUrlEncoder();
	private final Decoder base64dec = Base64.getUrlDecoder();
	/**
	 *  Stellt Kryptografische Funktionen für die Benutzerverwaltung, Passwortschult, Apikey Generation u.v.m. Bereit.
	 * @param controller der Controller, da zugriff z.b. auf den Radom Generator benötigt wird.
	 */
	public SecureController(final Controller controller) {
		this.random = controller.getRandom();
		try {
			securekeyfactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		final int addPWIterations = 200;
		try {
			Integer.parseInt(controller.getConfig("addPWIterations"));
		}catch(Exception e){
			System.out.println("Error reading addPWIterations from config" + e.getMessage() + "\n" + e);
		}
		iterations = random.getInt(MINITERATIONS + addPWIterations, MAXITERATIONS + addPWIterations);
	}
	/**
	 * Generiert ein Passwort, welches um einen Internen String ergänzt wird (pepper) und anschließend gesalzt und gehast wird.
	 * @param passwd das Klartext Passwort.
	 * @return anzahl der Interations:Salt:Hash
	 */
	public String generatePW(String passwd) {
		final char[] pwAsChars = (PEPPER + passwd).toCharArray();
		final byte[] salt = random.getSalt();
		byte[] hash = generatePW(pwAsChars, salt, iterations);
		return iterations + ":" +  Encode(salt) + ":" + Encode(hash);
	}
	/**
	 * Vergleicht ein Klartext passwort mit einem vorliegenden hash String welcher anzahl die in der Iterations, Pepper und Salt Enthält.
	 * @param passwd das klartext Passwort,
	 * @param pwhash anzahl der Interations:Salt:Hash
	 * @return true wenn es sich um das richtige Passwort gehandelt hat.
	 */
	public boolean checkPasswd(String passwd, String pwhash) {
		String[] hashcomps = pwhash.split(":");
		int iterations = Integer.parseInt(hashcomps[0]);
        byte[] salt = Decode(hashcomps[1]);
        byte[] hash = Decode(hashcomps[2]);
        byte[] testHash = generatePW((PEPPER + passwd).toCharArray(), salt, iterations);
        int diff = hash.length ^ testHash.length;
        for(int i = 0; i < hash.length && i < testHash.length; i++)
        {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
	}
	
	/**
	 * Hasht ein Passwort und verzieht es mit Salt
	 * @param passwd das klartext Passwort
	 * @param salt der Salz
	 * @param iterations anzahl der iterations
	 * @return der Passwort hash
	 */
	private byte[] generatePW(final char[] passwd,final byte[] salt, final int iterations){
		try {
			PBEKeySpec spec = new PBEKeySpec(passwd, salt, iterations, PWKEYLENGTH);
			return securekeyfactory.generateSecret(spec).getEncoded();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Base 64 Dekodieren
	 * @param src der Quellstring
	 * @return die Daten
	 */
	public byte[] Decode(final String src) {
		return base64dec.decode(src);
	}
	/**
	 * Base 64 Encode
	 * @param src die Daten
	 * @return der String
	 */
	public String Encode(final byte[] src) {
		return base64enc.encodeToString(src);
	}


}
