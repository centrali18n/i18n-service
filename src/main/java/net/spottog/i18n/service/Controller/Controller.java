package net.spottog.i18n.service.Controller;

import net.spottog.i18n.TextSave.SearchReturnSave;
import net.spottog.i18n.global.I18n;
import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;
import net.spottog.i18n.service.Model.DatabaseConnection;
import net.spottog.i18n.service.Model.ITextResponder;
import net.spottog.i18n.service.Model.User;
import net.spottog.i18n.service.Model.UserRight;

	/**
	 * Speicher für die einzelnen Controller.
	 * Bei der initialisierung werden die Einzelnen Controller ebenfalls initialisert. 
	 * Verwendet wird die klasse als Singleton.
	 *
	 */
public class Controller {
	private static Controller instance = new Controller();
	private ConfigController config = new ConfigController(this);
	private RandomGenerator random = new RandomGenerator(this);
	private SecureController secure = new SecureController(this);
	private KeyController keycontroller = new KeyController(this);
	private UserController usercontroller = new UserController(this);

	public static Controller getInstance() {
		return instance;
	}

	private Controller() {
		// Init Local
		I18n.Save = new LocalTextSave(new SearchReturnSave(null, true));
	}
	/**
	 * Beantwortet einen TextWithLanguageRequest
	 * @param request die Suchanfrage
	 * @return die Antwort.
	 */
	public ITextWithLanguageAnswer getText(TextWithLanguageRequest request) {
		for (ITextResponder controller : getKeyController().TextRespondersFromKey(request.getApiKey())) {
			ITextWithLanguageAnswer ret = controller.search(request);
			if (ret != null)
				return ret;
		}
		return null;
	}
	/**
	 * der RandomGenerator erzeugt zufallsdaten in verschieden formaten.
	 * @return ein initialisierter Ranomgenerator.
	 */
	public RandomGenerator getRandom() {
		return random;
	}
	/**
	 * Verwaltet die ApiKeys, welche bestimmen welche Text Speicher in welcher reinfolge befragt werden.
	 * @return ein initialiserter Key Controller.
	 */
	public KeyController getKeyController() {
		return keycontroller;
	}
	/**
	 * Gibt den Api Key Controller nur zurück, wenn der Benutzer die Berechtigung hat auf diese zuzugreifen.
	 * @param user der User, welcher den Controller anfordert.
	 * @return den Key Controller
	 */
	public KeyController getKeyController(User user) {
		if (user == null || !user.hasRight(UserRight.editKeys, null, 0))
			IllegalUnauthorizedAccessError(null);
		return getKeyController();
	}
	/**
	 * Gibt den Controller zurück, welcher auf Kryptografische Funktionen zur verfügung stellt.
	 * @return den Controller
	 */
	public SecureController getSecure() {
		return secure;
	}
	/**
	 * der Benutzer Controller ist dafür gedacht, um Die Benutzer zu verwalten.
	 * @return den Controller
	 */
	public UserController getUserController() {
		return usercontroller;
	}
	/**
	 * Zugriff auf ein einelnen Konfigurationseintrag aus dem Key Value Speicher.
	 * @param key der Key
	 * @return das zugehörige ELement.
	 */
	public String getConfig(final String key) {
		return config.get(key);
	}
	/**
	 * Verzögert die Ausführung um broot force angriffe auszubremsen und wirft dann eine exception wenn eine Berechtigung gefehlt hat.
	 * @param i18n darf null sein.
	 */
	public void IllegalUnauthorizedAccessError(I18n i18n) {
		if (null == i18n)
			i18n = new I18n("No authorization");
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		throw new net.spottog.i18n.service.Model.Transport.EditText.IllegalUnauthorizedAccessError(i18n);
	}

	/**
	 * Beim Beenden des Controllers werden alle Datenbank Verbindungen Geschlossen.
	 */
	@Override
	protected void finalize() throws Throwable {
		DatabaseConnection.closeAll();
	}

}
