package net.spottog.i18n.service.Controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Function;
import java.util.stream.Stream;

import net.spottog.i18n.global.I18n;
import net.spottog.i18n.service.Model.ApiKeyValue;
import net.spottog.i18n.service.Model.DatabaseConnection;
import net.spottog.i18n.service.Model.User;
import net.spottog.i18n.service.Model.Transport.EditText.ApiKeyResult;
/**
 * Verwaltet die ApiKeys, welche bestimmen welche Text Speicher in welcher reinfolge befragt werden.
 */
public class KeyController {
	private  final ConcurrentHashMap<String, AbstractSet<ApiKeyValue>> KeySave = new ConcurrentHashMap<String, AbstractSet<ApiKeyValue>>();
	private final Controller controller;
	/**
	 * Erzeugt einen Key Controller
	 * @param controller Zugriff auf die Controller.
	 */
	public KeyController(final Controller controller) {
		this.controller = controller;
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final Statement st = con.createStatement();
				final ResultSet rs = st.executeQuery("SELECT \"key\", \"sort\", \"TextSaveID\" FROM \"apikey\"");) {
			while (rs.next()) {
				final String key = rs.getString("key");
				final int sort = rs.getInt("sort");
				final Integer TextSaveID = rs.getInt("TextSaveID");
				AddOrReplaceLocale(key, new ApiKeyValue(sort, TextSaveID));
			}
		} catch (SQLException e) {
			System.out.println("Error im TextCotroller: " + e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * Erzeugt einen Neuen Api Key und legt diesen An.
	 * Der Textsave welcher Übergeben wird steht in der Sortierung an erster Stelle, da ein neuer Api key noch keine Weiteren Text Save's haben kann.
	 * @param SaveID der TextSave mit der Sortierung 1
	 * @return der Key mit weiteren Details.
	 * @throws SQLException sollte nicht vorkommen.
	 */
	public synchronized ApiKeyResult addApiKey(final Integer SaveID) throws SQLException {
		String Apikey;
		final int sort = 1;
		do {
			Apikey = controller.getRandom().getNextApiKey();
		}while(KeySave.containsKey(Apikey));	// Zwar unrealistisch trotzdem kurz Prüfen, ob es den Key nicht schon gibt.
		String sql = "INSERT INTO \"apikey\" (\"key\", \"sort\", \"TextSaveID\") VALUES (?, ?, ?)";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setString(++index, Apikey);
			st.setInt(++index, sort);
			st.setInt(++index, SaveID);
			st.executeUpdate();
			AddOrReplaceLocale(Apikey, new ApiKeyValue(1, SaveID));
			return new ApiKeyResult(Apikey, SaveID,TextController.Get(SaveID).getName() ,sort);
		}
	}
	/**
	 * fügt einen Api Key dem Lokalen Speicher hinzu.
	 * exestiert er schon wird er Ersetzt.
	 * @param ApiKey der Key
	 * @param value das Element.
	 */
	private void AddOrReplaceLocale(final String ApiKey, final ApiKeyValue value) {
		final AbstractSet<ApiKeyValue> keySave = KeySave.computeIfAbsent(ApiKey,
				new Function<String, AbstractSet<ApiKeyValue>>() {
					public AbstractSet<ApiKeyValue> apply(final String ApiKLey) {
						return new ConcurrentSkipListSet<ApiKeyValue>();
					}
				});
		keySave.add(value);
	}
	/**
	 * Gibt eine Sortiete Liste zurück, in welcher alle Text Save's sind, welche zu dem Api Key gehören.
	 * @param apikey der Api Key
	 * @return die Sortiete liste.
	 */
	public AbstractSet<ApiKeyValue> TextRespondersFromKey(String apikey) {
		AbstractSet<ApiKeyValue> keysave = null;
		if(null != apikey && apikey.length() == RandomGenerator.APIKEYLENGTHLENGTH) {
			keysave = KeySave.get(apikey);
		}
		if(null == keysave)
			controller.IllegalUnauthorizedAccessError(new I18n("ApiKey not Found"));
		return keysave;
	}
	/**
	 * Fügt einem Api Key, einen Weiteren Text Save hinzu.
	 * @param apiKey der Api key
	 * @param textSaveID der Text Speicher
	 * @param sort gewünschte Sortierung. Wenn Belegt, wird es dahinter gestellt.
	 * @return das Objekt, welche alle tatzächlichen werte enthält.
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public synchronized ApiKeyResult addToApiKey(final String apiKey, final int textSaveID,final int sort) throws SQLException {
		final AbstractSet<ApiKeyValue> keysave = KeySave.get(apiKey);
		if(null == keysave)
			controller.IllegalUnauthorizedAccessError(new I18n("ApiKey not Found"));
		ApiKeyValue newValue = new ApiKeyValue(sort, textSaveID);
		while(keysave.contains(newValue)) {
			newValue = new ApiKeyValue(newValue.getSort() + 1, textSaveID);
		}
		String sql = "INSERT INTO \"apikey\" (\"key\", \"sort\", \"TextSaveID\") VALUES (?, ?, ?)";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setString(++index, apiKey);
			st.setInt(++index, newValue.getSort());
			st.setInt(++index, newValue.getSaveID());
			st.executeUpdate();
			keysave.add(newValue);
			return new ApiKeyResult(apiKey, newValue.getSaveID(),TextController.Get(newValue.getSaveID()).getName() ,newValue.getSort());
		}
	}
	/**
	 * Entfernt einen Textsave aus einem Api Key.
	 * Sollte der Api Key anschließend keine Text Speicher mehr enthalten wird er komplett gelöscht.
	 * @param apiKey Der Api key welcher Bearbeitet werden soll
	 * @param Saveid id des Text Speichers, welcher aus dem Key Entfernt wird.
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public void delApiKey(final String apiKey, Integer Saveid) throws SQLException {
		String sql = "DELETE FROM \"apikey\" WHERE  \"key\" = ? AND \"TextSaveID\" = ?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setString(++index, apiKey);
			st.setInt(++index, Saveid);
			st.executeUpdate();
			AbstractSet<ApiKeyValue> set =  KeySave.getOrDefault(apiKey, null);
			set.removeIf(e -> e.getSaveID().equals(Saveid));
			if(set.size() == 0)
				KeySave.remove((Object)set);
		}
	}
	/**
	 * Gibt alle Keys, incl Textspeicher zurück, auf welche ein Benutzer zugriff hat.
	 * @param user der User.
	 * @return Keys, incl Textspeicher
	 */
	public Stream<Entry<String, AbstractSet<ApiKeyValue>>> getKeysForUser(User user){
		final Collection<Integer> ids = user.getSaveIDs();
		return KeySave.entrySet().stream().filter(element -> {
				for(ApiKeyValue akv : element.getValue()) {
					if(ids.contains(akv.getSaveID()))
						return true;
				}
				return false;
		});
	}
	/**
	 * Entfernt einen Textsave aus allen Api Keys.
	 * @param SaveID die id des Text Speichers.
	 * @throws SQLException Fehler in der Datenbank.
	 */
	public void RemoveTextSave(int SaveID) throws SQLException {
		final String sql = "DELETE FROM \"apikey\" WHERE \"TextSaveID\"=?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			st.setInt(1, SaveID);
			st.executeUpdate();
			for(AbstractSet<ApiKeyValue> apikey : KeySave.values()) {
				apikey.removeIf(ak -> ak.getSaveID() == SaveID);
			}
		}
	}
}
