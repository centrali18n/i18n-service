package net.spottog.i18n.service.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import net.spottog.i18n.service.Model.DatabaseConnection;
/**
 * Zugriff auf die Config tabelle in der Datenbank.
 * Die Datenbank ist Als Key Value Speicher aufgebaut.
 */
public class ConfigController {
	private final Map<String, String> save = new HashMap<String, String>();
	/**
	 * Ließt die datenbank ein und Initialisert den Config Controller
	 * @param controller Controller welcher die Verrschiedenen Controller beheimatet.
	 */
	public ConfigController(final Controller controller) {
		final String sql = "SELECT \"Key\", \"Value\" FROM \"Config\"";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final Statement st = con.createStatement();
				final ResultSet rs = st.executeQuery(sql);) {
			while (rs.next()) {
				save.put(rs.getString("Key"), rs.getString("Value"));
			}

		} catch (SQLException e) {
			System.out.println("Error in Config Controller " + e);
		}
	}
	/**
	 * Frägt einen Einzelnen Konfigurationsparameter ab.
	 * @param key Key in der Konfigurationstabelle.
	 * @return der wert von dem Key Value paar.
	 */
	public String get(final String key) {
		return save.get(key);
	}
}
