package net.spottog.i18n.service.Rest;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Model.CockieObjects;
import net.spottog.i18n.service.Model.Transport.EditText.ApiKeyRequest;
import net.spottog.i18n.service.Model.Transport.EditText.ApiKeyResult;
import net.spottog.i18n.service.Model.Transport.EditText.EditApiKeyRequest;
import net.spottog.i18n.service.Model.Transport.EditText.ToApiKeyRequest;
/**
 * Schnittstellen, Funktionen um api keys zu Bearbeiten
 */
@Path("/ApiKey")
public class ApiKey {
	private static Controller controller = Controller.getInstance();

	
	/**
	 * Erzeugt einen Neuen Api Key
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param apikeyrequest Json kodierter request, welche die anfrage enthält.
	 * @return Informationen über den neuen Api Schlüssel.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public ApiKeyResult NewApiKey(@Context HttpServletRequest req, final ApiKeyRequest apikeyrequest) throws SQLException {
      return controller.getKeyController(CockieObjects.USER.get(req)).addApiKey(apikeyrequest.getTextsaveid());
    }
	/**
	 * Fügt einem Api key einen weiteren Text Speicher hinzu.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param apikeyrequest Json kodierter request, welche die anfrage enthält.
	 * @return Informationen über den neuen Api Schlüssel.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@PUT
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public ApiKeyResult addToApiKey(@Context HttpServletRequest req, final ToApiKeyRequest apikeyrequest) throws SQLException {
      return controller.getKeyController(CockieObjects.USER.get(req)).addToApiKey(apikeyrequest.getApikey(), apikeyrequest.getTextsaveid(), apikeyrequest.getSort());
    }
	
	/**
	 * Löscht einen Api Schlüssel.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param apikeyrequest Json kodierter request, welche die anfrage enthält.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteApiKey(@Context HttpServletRequest req, final EditApiKeyRequest apikeyrequest) throws SQLException {
      controller.getKeyController(CockieObjects.USER.get(req)).delApiKey(apikeyrequest.getApikey(), apikeyrequest.getTextsaveid());
    }
}
