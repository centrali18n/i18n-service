package net.spottog.i18n.service.Rest;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.spottog.i18n.global.I18n;
import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Controller.UserController;
import net.spottog.i18n.service.Model.CockieObjects;
import net.spottog.i18n.service.Model.UserRight;
import net.spottog.i18n.service.Model.Transport.EditText.UnsupportedStateException;
import net.spottog.i18n.service.Model.Transport.EditText.UserPasswdRequest;
import net.spottog.i18n.service.Model.Transport.EditText.UserRequest;
import net.spottog.i18n.service.Model.Transport.EditText.UserRightChangeRequest;
/**
 * Rest Schnittstelle zur benutzerverwaltung.
 */
@Path("/user")
public class User {
	private final static Controller controller = Controller.getInstance();
	private final static UserController usercontroller = controller.getUserController();
	/**
	 * Einen neuen Benutzer anlegen.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param userrequest Json kodierter request, welche die anfrage enthält.
	 * @return das Benutzer Element
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public net.spottog.i18n.service.Model.User NewUser(@Context HttpServletRequest req,final UserRequest userrequest) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.editUser, null, 0))
			controller.IllegalUnauthorizedAccessError(new I18n("I can not do anything with these access data."));
		return usercontroller.addUser( userrequest.getEmail(), userrequest.getPasswd());
	}
	/**
	 * Über diese Schnitstelle kann sich ein Benutzer einlogen, dabei wird das Benutzer Element nicht nur zurückgegeben, es wird auch gleich im Cockie bzw. in der Java Session Gespeichert.
	 * @param req Http Informationen, zum zugriff auf das Cockie
	 * @param userrequest Json kodierter request, welche die anfrage enthält.
	 * @return das Benutzer element
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public net.spottog.i18n.service.Model.User LoginUser(@Context HttpServletRequest req, final UserRequest userrequest) throws SQLException {
		net.spottog.i18n.service.Model.User user = usercontroller.LoginUser(userrequest.getEmail(), userrequest.getPasswd());
		CockieObjects.USER.set(req, user);
		return user;
	}
	/**
	 * Das Passwort eines Benutzers ändern.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param userrequest Json kodierter request, welche die anfrage enthält.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@POST
	@Path("/setpasswd/")
    @Consumes(MediaType.APPLICATION_JSON)
	public void SetUserPasswd(@Context HttpServletRequest req, final UserPasswdRequest userrequest) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.editUser, null, 0))
			controller.IllegalUnauthorizedAccessError(new I18n("I can not do anything with these access data."));
		usercontroller.setUserPasswd(usercontroller.getUserFromId(userrequest.getUserid()), userrequest.getPasswd());
	}
	/**
	 * Einem Benutzer ein zusätzliches recht geben.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param usrcr Json kodierter request, welche die anfrage enthält.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@POST
	@Path("/adduserright/")
    @Consumes(MediaType.APPLICATION_JSON)
	public void addUserRight(@Context HttpServletRequest req, final UserRightChangeRequest usrcr) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.editUser, null, 0))
			controller.IllegalUnauthorizedAccessError(new I18n("I can not do anything with these access data."));
		usercontroller.getUserFromId(usrcr.getUserid()).CreateNewRight(usrcr.getUserright(), usrcr.getLanguage(), usrcr.getSaveid());
	}
	/**
	 * Einem Benutzer ein benutzerrecht entziehen.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param usrcr Json kodierter request, welche die anfrage enthält.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@POST
	@Path("/removeuserright/")
    @Consumes(MediaType.APPLICATION_JSON)
	public void removeUserRight(@Context HttpServletRequest req, final UserRightChangeRequest usrcr) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.editUser, null, 0))
			controller.IllegalUnauthorizedAccessError(new I18n("I can not do anything with these access data."));
		if(usrcr.getUserright() == UserRight.editUser && !(usercontroller.countUserWithEditUserRights() > 1)) {
			throw new UnsupportedStateException(new I18n("no Delete Last User Error"));
		}
		else {
			usercontroller.getUserFromId(usrcr.getUserid()).RemoveRight(usrcr.getUserright(), null, usrcr.getSaveid());
		}
	}
	/**
	 * sich selbst auslogen, und damit das Cockie bzw. die Session für ungültig erklären
	 * @param req Http Informationen für den zugriff auf das Cockie
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@GET
	@Path("/logout/")
	public void LogoutFromTokken(@Context HttpServletRequest req) throws SQLException {
		req.getSession().invalidate();
	}
	/**
	 * Löscht einen Benutzer
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param userToDelete Json kodierter request, welche die anfrage enthält.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@DELETE
	public void DeleteUser(@Context HttpServletRequest req, final Integer userToDelete) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.editUser, null, 0))
			controller.IllegalUnauthorizedAccessError(new I18n("I can not do anything with these access data."));
		usercontroller.DelUser(userToDelete);
	}
}
