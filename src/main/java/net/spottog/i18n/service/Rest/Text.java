package net.spottog.i18n.service.Rest;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;
import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Controller.TextController;
import net.spottog.i18n.service.Model.CockieObjects;
import net.spottog.i18n.service.Model.UserRight;
import net.spottog.i18n.service.Model.Transport.EditText.EditTextTransport;
import net.spottog.i18n.service.Model.Transport.EditText.NewTextSaveTrasport;
import net.spottog.i18n.service.Model.Transport.EditText.TextElement;

/**
 * Abfrage bzw. Bearbeiten der Texte
 */
@Path("/text")
public class Text {

	private static Controller controller = Controller.getInstance();

	/**
	 * Einen Text Abrufen
	 * @param request die Json Codierte Abfrage.
	 * @return das Übersetzte Text Element
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ITextWithLanguageAnswer getText(TextWithLanguageRequest request) {
		return controller.getText(request);
//		System.out.println("Anfrage: " + request.getLanguage() + " : " + request.getSearchString() + "     :" + request.getApiKey());
//		ITextWithLanguageAnswer ans = controller.getText(request);
//		System.out.println("antwort: " + ans.getLanguage() + " : " + ans.getText() + "     :" + ans.getExpire());
//		return ans;
	}
	/**
	 * Ein Text Eintrag bearbeiten.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param request die Json Codierte Anfrage.
	 */
	@POST
	@Path("/edit/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void EditText(@Context HttpServletRequest req, EditTextTransport request) {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		TextController textcontroller = TextController.Get(request.getSaveid());
		for (TextElement ie : request.getToedit()) {
			if (user.hasRight(UserRight.writeText, ie.getLanguage(), request.getSaveid())) {
				textcontroller.AddOrReplace(TextController.StringToLocale(ie.getLanguage()), request.getSearchstring(),
						ie.getText(), ie.getExpire());
			}
		}
	}
	/**
	 * Ein Text element löschen.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param request die Json Codierte Anfrage.
	 */
	@DELETE
	@Path("/edit/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void DeleteText(@Context HttpServletRequest req, EditTextTransport request) {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.writeText, null, request.getSaveid())) {
			controller.IllegalUnauthorizedAccessError(null);
		}
		TextController textcontroller = TextController.Get(request.getSaveid());
		textcontroller.Delete(request.getSearchstring());
	}
	/**
	 * Einen neuen Texte Speicher Anlegen.
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param request die Json Codierte Abfrage.
	 * @return die eindeutige id des neu angelegten Text Speicher.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@POST
	@Path("/newSave/")
	@Consumes(MediaType.APPLICATION_JSON)
	public int newTextSave(@Context HttpServletRequest req, final NewTextSaveTrasport request) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if (user == null || !user.hasRight(UserRight.editUser, null, 0))
			Controller.getInstance().IllegalUnauthorizedAccessError(null);
		return TextController.CreateNew(request.getName(), user, request.getDefaultexpire());
	}
	/**
	 * Löscht einen Texte Speicher
	 * @param req Http Informationen es muss das User Cockie vorhanden sein.
	 * @param SaveID die eindeutige id des zu löschenden Text Speichers.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	@DELETE
	@Path("/delSave/")
	public void DelTextSave(@Context HttpServletRequest req, Integer SaveID) throws SQLException {
		net.spottog.i18n.service.Model.User user = CockieObjects.USER.get(req);
		if(user == null || !user.hasRight(UserRight.editUser, null, SaveID)) {
			controller.IllegalUnauthorizedAccessError(null);
		}
		TextController.Delete(TextController.Get(SaveID));
	}
}
