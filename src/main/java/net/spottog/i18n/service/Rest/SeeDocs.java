package net.spottog.i18n.service.Rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Error page für die Rest Api.
 * je nachdem welche codierung angefragt wird, ist die antwort entsprechend codiert.
 */
@Path("/")
public class SeeDocs {

    // This method is called if TEXT_PLAIN is request
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String sayPlainTextreadDocs() {
      return "see the  Dokumentation";
    }

    // This method is called if XML is request
    @GET
    @Produces(MediaType.TEXT_XML)
    public String sayXMLreadDocs() {
      return "<?xml version=\"1.0\"?>\n<error>see the Dokumentation</error>\n<code>404</code>";
    }

    // This method is called if HTML is request
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String sayHtmlreadDocs() {
      return "<html><head><title>i18n-service</title></head><body>see the <a href=\"/doc/\">Dokumentation</a></body></html>";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String sayjsonreadDocs() {
      return "{\n  \"error\": \"see the  Dokumentation\",\n   \"code\": 404\n }";
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String sayaxmlDocs() {
      return sayaxmlDocs();
    }
}
