package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Transport Element um ein Text Speicher einem Api Key schlüssel hinzuzufügen.
 */
public class ToApiKeyRequest extends EditApiKeyRequest {
	private int sort;
	/**
	 * Sortierung, nach welcher reinfolge ein Text Speicher abgefragt wird.
	 * @return sortierung
	 */
	public int getSort() {
		return sort;
	}
	/**
	 * Sortierung, nach welcher reinfolge ein Text Speicher abgefragt wird.
	 * @param sort sortierung
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}
}
