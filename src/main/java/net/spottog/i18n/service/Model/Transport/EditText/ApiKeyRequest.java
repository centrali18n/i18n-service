package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * ÜBerträgt die id eines Text Speichers um einen Api Key abzufragen.
 */
public class ApiKeyRequest {
	private int textsaveid;
	/**
	 * Die id des Text Speichers
	 * @return die ID
	 */
	public int getTextsaveid() {
		return textsaveid;
	}
	/**
	 * Die id des Text Speichers
	 * @param textsaveid die ID
	 */
	public void setTextsaveid(int textsaveid) {
		this.textsaveid = textsaveid;
	}
}
