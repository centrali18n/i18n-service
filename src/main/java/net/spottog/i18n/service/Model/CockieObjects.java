package net.spottog.i18n.service.Model;

import javax.servlet.http.HttpServletRequest;
/**
 * Ermöglicht es den Benutzer als cookie im http objekt zu speichern und somit ein einfahes ein und auslogen
 * @param <T> Das zu Speichernde Objekt z.b. ein User Element.
 */
public final class CockieObjects<T> {
	public static final CockieObjects<User> USER = new CockieObjects<>("USER");
    private final String key;
	 
	private CockieObjects(String key) {
        this.key = key;
    }
    @Override
    public String toString() {
        return key;
    }
    /**
     * Setzt den Benutzer.
     * @param req der http request
     * @param o der Benutzer
     */
    public void set(HttpServletRequest req, T o) {
    	req.getSession().setAttribute(key, o);
    }
    /**
     * Frägt den Benutzer ab
     * @param req der http reuqest in welchem er gespeichert ist
     * @return das Benutzer objekt
     */
	@SuppressWarnings("unchecked")
	public T get(HttpServletRequest req) {
    	return (T)req.getSession().getAttribute(key);
    }
}
