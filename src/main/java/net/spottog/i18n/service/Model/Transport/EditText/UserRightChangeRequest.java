package net.spottog.i18n.service.Model.Transport.EditText;

import net.spottog.i18n.service.Model.UserRight;
/**
 * Transport element um die Benutzerrechte zu bearbieten
 */
public class UserRightChangeRequest {
	private Integer userid;
	private UserRight userright;
	private int saveid;
	private String language;
	/**
	 * Die Benutzer id
	 * @return id
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * Die Benutzer id
	 * @param userid id
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * Benutzer recht welches bearbeitet werden soll.
	 * @return das Benurterrecht
	 */
	public UserRight getUserright() {
		return userright;
	}
	/**
	 * Benutzer recht welches bearbeitet werden soll.
	 * @param userright das Benurterrecht
	 */
	public void setUserright(UserRight userright) {
		this.userright = userright;
	}
	/**
	 * Der Text Speicher
	 * @return id des Text Speichers
	 */
	public int getSaveid() {
		return saveid;
	}
	/**
	 * Der Text Speicher
	 * @param saveid id des Text Speichers
	 */
	public void setSaveid(int saveid) {
		this.saveid = saveid;
	}
	/**
	 * Die Sprache
	 * @return sprache
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * Die Sprache
	 * @param language sprache
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
}
