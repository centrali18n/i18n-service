package net.spottog.i18n.service.Model;

import java.io.FileReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Konfiguration der Datenbank.
 */
public class dbConfig {
	private final String url;

	String getURL() {
		return url;
	}

	private final String Passwd;

	String getPasswd() {
		return Passwd;
	}

	private final String User;

	String getUser() {
		return User;
	}

	private final String Classname;

	String getClassname() {
		return Classname;
	}

	public dbConfig(final String url, final String Passwd, final String User, final String Classname) {
		this.url = url;
		this.User = User;
		this.Passwd = Passwd;
		this.Classname = Classname;
	}

	public static dbConfig getCurrent() {
		return current;
	}

	private static final dbConfig current;
	static {
		dbConfig tmp = ReadFromFile("/etc/I18nService.json");
		if (tmp == null)
			tmp = ReadFromFile("C:\\I18nService.json");
		current = tmp;
	}

	private static dbConfig ReadFromFile(final String filePath) {
		JSONParser parser = new JSONParser();
		try {
			final FileReader fr = new FileReader(filePath);
			final JSONObject jsonObject = (JSONObject) parser.parse(fr);
			return new dbConfig((String) jsonObject.get("url"), (String) jsonObject.get("passwd"),
					(String) jsonObject.get("user"), (String) jsonObject.get("classname"));
		} catch (Exception e) {
			System.out.println("Exception beim lesen der zugangsdaten: " + e.getMessage());
			return null;
		}
	}

//	
//JSON EXAMPLE:
//{
//  "url": "jdbc:postgresql://192.168.10.21:5432/i18n",
//  "user": "i18n",
//  "passwd": "i18n",
//  "classname": "org.postgresql.Driver"
//}

}
