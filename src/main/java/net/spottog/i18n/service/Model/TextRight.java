package net.spottog.i18n.service.Model;

import java.io.Serializable;
import java.util.Locale;

import net.spottog.i18n.service.Controller.TextController;

/**
 * Das Benutzerrecht auf einen Text Speicher zuzugreifen.
 * Alle Werte können nicht bearbeitet werden.
 * Bei Änderungen muss ein neues Objekt erzeugt werden.
 */
public class TextRight implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 468831816664002802L;
	private final boolean write;
	private final Locale locale;
	private final int SaveID;
	/**
	 * Erzeugt ein Rechte Objekt.
	 * Alle Werte können nicht bearbeitet werden.
	 * @param write Schreibrechte oder Leserechte
	 * @param localeString die Sprache.
	 * @param SaveID der Text Speicher.
	 */
	public TextRight(final boolean write,final String localeString,final int SaveID) {
		this.write = write;
		this.SaveID = SaveID;
		if(localeString.length() < 2)
			locale = null;
		else
			locale = TextController.StringToLocale(localeString);
	}
	/**
	 * Erzeugt ein Rechte Objekt.
	 * Alle Werte können nicht bearbeitet werden.
	 * @param write Schreibrechte oder Leserechte
	 * @param locale die Sprache.
	 * @param SaveID der Text Speicher.
	 */
	public TextRight(final boolean write, final Locale locale, final int SaveID) {
		this.write = write;
		this.SaveID = SaveID;
		this.locale = locale;
	}
	/**
	 * Schreib oder Leserechte
	 * @return true wenn schreibrechte bestehen und false wenn leserechte bestehen
	 */
	public boolean canWrite() {
		return write;
	}
	/**
	 * Die Sprache
	 * @return die Sprache
	 */
	public Locale getLocale() {
		return locale;
	}
	/**
	 * Der Text Speicher
	 * @return die ID des Text spreichers.
	 */
	public int getSaveID() {
		return SaveID;
	}

}
