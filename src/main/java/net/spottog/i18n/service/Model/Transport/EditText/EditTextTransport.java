package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Transportobjekt um text elemente zu bearbeiten.
 */
public class EditTextTransport {
	private String searchstring;
	private int saveid;
	private TextElement[] toedit;
	/**
	 * Index des zu Suchenden Text, bzw. ein text der diesen beschreibt.
	 * @return der Text
	 */
	public String getSearchstring() {
		return searchstring;
	}
	/**
	 * Index des zu Suchenden Text, bzw. ein text der diesen beschreibt.
	 * @param searchstring der Text
	 */
	public void setSearchstring(String searchstring) {
		this.searchstring = searchstring;
	}
	/**
	 * Auflistung der zu bearbeitenden Text Elemente
	 * @return die elemente
	 */
	public TextElement[] getToedit() {
		return toedit;
	}
	/**
	 * Auflistung der zu bearbeitenden Text Elemente
	 * @param toedit die elemente
	 */
	public void setToedit(TextElement[] toedit) {
		this.toedit = toedit;
	}
	/**
	 * Der Text Speicher
	 * @return due id
	 */
	public int getSaveid() {
		return saveid;
	}
	/**
	 * Der Text Speicher
	 * @param saveid die id
	 */
	public void setSaveid(int saveid) {
		this.saveid = saveid;
	}
}
