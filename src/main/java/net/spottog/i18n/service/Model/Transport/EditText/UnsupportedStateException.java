package net.spottog.i18n.service.Model.Transport.EditText;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;

import net.spottog.i18n.global.I18n;
/**
 * Fehler, wenn eine Aktion zu einem Unsuporteten zustand führen würde und deshalb nicht ausgeführt wird.
 * Http Status: 400 Bad Request
 */
public class UnsupportedStateException extends WebApplicationException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1021795985423419144L;
	/**
	 * Fehler, wenn eine Aktion zu einem Unsuporteten zustand führen würde und deshalb nicht ausgeführt wird.
	 * Http Status: 400 Bad Request
	 */
	public UnsupportedStateException() {
        super(Response.status(Status.BAD_REQUEST).build());
    }
	/**
	 * Fehler, wenn eine Aktion zu einem Unsuporteten zustand führen würde und deshalb nicht ausgeführt wird.
	 * Http Status: 400 Bad Request
	 * @param message Fehlermeldung die zusätzlich zurück gegeben wird.
	 */
    public UnsupportedStateException(I18n message) {
        super(Response.status(Status.BAD_REQUEST).entity(message.toString()).type("text/plain").build());
    }
}
