package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Der Api Key Request beinhaltet einen Text Speicher, jetzt kommt noch der Api Key dazu
 */
public class EditApiKeyRequest extends ApiKeyRequest {
	private String apikey;
	/**
	 * Der Api key mit welchem über die json rest api auf den service zugegriffen werden kann.
	 * @return der key
	 */
	public String getApikey() {
		return apikey;
	}
	/**
	 * Der Api key mit welchem über die json rest api auf den service zugegriffen werden kann.
	 * @param apikey der key
	 */
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
}
