package net.spottog.i18n.service.Model;
import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
/**
 * Enables the use of {@code try-with-resources} with {@code ReadWriteLock}.
 *
 * @author Gili Tzabari + Fabian Spottog
 * @see <a href="https://stackoverflow.com/a/46248923/14731">Related code on Stackoverflow</a>
 * https://cmsdk.com/java/use-trywithresources-with-readwritelock-on-hold.html
 */
public final class ReadWriteLockAsResource implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5583057311157997231L;
	private final ReadWriteLock lock;
    public ReadWriteLockAsResource() {
    	this(new ReentrantReadWriteLock());
    }
    /**
     * @param lock a lock
     * @throws NullPointerException if {@code lock} is null
     */
    public ReadWriteLockAsResource(ReadWriteLock lock)
    {
        if (lock == null)
          throw new NullPointerException("lock may not be null");
        this.lock = lock;
    }
    /**
     * Starts a new read-lock.
     *
     * @return the read-lock as a resource
     */
    public CloseableLock readLock()
    {
        Lock readLock = lock.readLock();
        readLock.lock();
        return readLock::unlock;
    }
    /**
     * Starts a new write-lock.
     *
     * @return the write-lock as a resource
     */
    public CloseableLock writeLock()
    {
        Lock writeLock = lock.writeLock();
        writeLock.lock();
        return writeLock::unlock;
    }
}
/**
 * @author Gili Tzabari
 * @see <a href="https://stackoverflow.com/a/46248923/14731">Related code on Stackoverflow</a>
 */
interface CloseableLock extends AutoCloseable
{
    /**
     * Releases the lock.
     */
    @Override
    void close();
}