package net.spottog.i18n.service.Model;
/**
 * Ein Benutzer recht, welches ein Benutzer haben kann oder auch nicht.
 */
public enum UserRight{
	/**
	 * Das Recht Benutzer anzulegen oder diese zu bearbeiten.
	 */
	editUser,
	/**
	 * Das Recht die Api keys zu bearbeiten.
	 */
	editKeys,
	/**
	 * Das recht ein Datenbank Backup zu machen.
	 * Damit ist ein Direkter zugriff auf die Datenbank möglich.
	 */
	backup,
	/**
	 * Das Recht ein Text aus einem Text Speicher in einer Bestimmten Sprache zu lesen
	 */
	readText,
	/**
	 * Das Recht Schreibend in einer Bestimmten sprache auf einen textspiecher zuzugreifen.
	 */
	writeText
}

