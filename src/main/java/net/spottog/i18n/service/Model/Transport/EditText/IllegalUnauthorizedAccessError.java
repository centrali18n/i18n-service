package net.spottog.i18n.service.Model.Transport.EditText;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;

import net.spottog.i18n.global.I18n;
/**
 * Fehler wenn versucht wurde auf Eine resurce zuzugreifen für die man keine Berechtigung hat.
 */
public class IllegalUnauthorizedAccessError extends WebApplicationException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1021795076997419163L;
	/**
	 * Erzeugt einen 401 Unauthorized http Fehler
	 */
	public IllegalUnauthorizedAccessError() {
        super(Response.status(Status.UNAUTHORIZED).build());
    }
	/**
	 * Erzeugt einen 401 Unauthorized http Fehler
	 * @param message der Text, welcher zurückgegeben wird.
	 */
    public IllegalUnauthorizedAccessError(I18n message) {
        super(Response.status(Status.UNAUTHORIZED).entity(message.toString()).type("text/plain").build());
    }
}
