package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Transport Element für einen Neuen Text Speicher
 */
public class NewTextSaveTrasport {
	private String name;
	private int defaultexpire;
	/**
	 * Der Name des anzulegenden Text Speichers.
	 * @return der Name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Der Name des anzulegenden Text Speichers.
	 * @param name der Name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Standard wert für die Zeit nach der ein Text Abläuft.
	 * @return die zeit
	 */
	public int getDefaultexpire() {
		return defaultexpire;
	}
	/**
	 * Standard wert für die Zeit nach der ein Text Abläuft.
	 * @param defaultexpire die Zeit
	 */
	public void setDefaultexpire(int defaultexpire) {
		this.defaultexpire = defaultexpire;
	}
}
