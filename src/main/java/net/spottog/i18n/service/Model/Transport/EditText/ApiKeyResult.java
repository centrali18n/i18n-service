package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * TextSave Api Key Entity.
 */
public class ApiKeyResult {
	private final String apikey;
	private final int saveid;
	private final String listname;
	private final int sort;
	/**
	 * Erzeugt ein Objekt bei welchem die Werte sich anschließend nicht mehr verändern können
	 * @param apikey der api key welcher mehrere TextSave's Sortiert enthält.
	 * @param saveid Eine Textsave
	 * @param listname Beschreibender Name
	 * @param sort Sortierung.
	 */
	public ApiKeyResult(final String apikey, final int saveid, final String listname, final int sort) {
		this.apikey = apikey;
		this.saveid = saveid;
		this.listname = listname;
		this.sort = sort;
	}
	/**
	 * apikey der api key welcher mehrere TextSave's Sortiert enthält.
	 * @return key als String
	 */
	public String getApikey() {
		return apikey;
	}
	/**
	 * Eine Textsave
	 * @return die id
	 */
	public int getSaveid() {
		return saveid;
	}
	/**
	 * Beschreibender Name
	 * @return der Name zum Besseren Verständnis
	 */
	public String getListname() {
		return listname;
	}
	/**
	 * Sortierung
	 * @return als int
	 */
	public int getSort() {
		return sort;
	}
}
