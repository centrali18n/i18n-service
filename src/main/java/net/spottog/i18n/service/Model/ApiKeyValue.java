package net.spottog.i18n.service.Model;

import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;
import net.spottog.i18n.service.Controller.TextController;
/**
 * Ein TextSave Element, welches sich in einen Api Key sortieren lässt.
 */
public class ApiKeyValue implements Comparable<ApiKeyValue>, ITextResponder {
	private final int Sort;
	private final Integer SaveID;
	/**
	 * Erzeugt ein TextSave Element, welches sich in einen Api Key sortieren lässt.	 
	 * @param Sort Sortierung.
	 * @param SaveID der Zugehörige TextSave muss vorhanden und über den TextController verfügbar sein.
	 */
	public ApiKeyValue(final int Sort, final Integer SaveID) {
		this.Sort = Sort;
		this.SaveID = SaveID;
	}
	/**
	 * Gibt die Save ID Zurück.
	 * @return TextSave ID
	 */
	public Integer getSaveID() {
		return SaveID;
	}
	/**
	 * Die Sortierung innerhalb des Api Keys.
	 * @return eine Zahl, welche der Sortierung Entspricht.
	 */
	public int getSort() {
		return Sort;
	}
	/**
	 * Vergleicht das Element anhand der Sortierung mit einem Anderen ApiKeyValue.
	 */
	@Override
	public int compareTo(ApiKeyValue other) {
		return Sort - other.Sort;
	}
	/**
	 * Der Zugehgörige Text Save
	 * @return Der Zugehgörige Text Save
	 */
	private TextController getController() {
		return TextController.Get(getSaveID());
	}

	@Override
	public ITextWithLanguageAnswer search(TextWithLanguageRequest request) {
		return getController().search(request);
	}
}
