package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Transport element, welches die email adresse und das passwort überträgt.
 */
public class UserRequest {
	private String email;
	private String passwd;
	/**
	 * Die Email Adresse.
	 * @return email Die Email Adresse.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Die Email Adresse
	 * @param email die Email Adresse.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Das Passwort
	 * @return password
	 */
	public String getPasswd() {
		return passwd;
	}
	/**
	 * Das Passwort
	 * @param passwd passwort
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
}
