package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Transpoort element für einen einzelnen Text.
 */
public class TextElement {
	private String language;
	private String text;
	private int expire;
	/**
	 * Ablaufzeit des textes.
	 * @return die zeit
	 */
	public int getExpire() {
		return expire;
	}
	/**
	 * Ablaufzeit des Textes
	 * @param expire die Zeit
	 */
	public void setExpire(int expire) {
		this.expire = expire;
	}
	/**
	 * Sprache des Textes
	 * @return die Sprache
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * Sprache des textes
	 * @param language die Sprache
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * Der Text
	 * @return text
	 */
	public String getText() {
		return text;
	}
	/**
	 * Der Text
	 * @param text text
	 */
	public void setText(String text) {
		this.text = text;
	}
}
