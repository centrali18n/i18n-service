package net.spottog.i18n.service.Model;

import java.util.Locale;

/**
 * Ein Locale Objekt auf welches man Lese oder Schreibe rechte haben kann. Die
 * Eigenschaften können sich nicht ändern. Ist da notwendig muss ein neues
 * Objekt erzeugt werden,
 */
public class LocaleWithWriteAccess implements Comparable<LocaleWithWriteAccess> {
	private final Locale Language;
	private final boolean writeAccess;

	/**
	 * Erzeugt ein Objekt. Die Eigenschaften können sich nicht ändern. Ist da
	 * notwendig muss ein neues Objekt erzeugt werden,
	 * @param Language die Sprache.
	 * @param writeAccess true bei Schreibrechten false bei Leserechten.
	 */
	public LocaleWithWriteAccess(final Locale Language, final boolean writeAccess) {
		this.Language = Language;
		this.writeAccess = writeAccess;
	}
	/**
	 * Die Sprache
	 * @return Die Sprache
	 */
	public Locale getLanguage() {
		return Language;
	}
	/**
	 * Schreib oder Lese Rechte
	 * @return true bei Schreibrechten false bei Leserechten.
	 */
	public boolean isWriteAccess() {
		return writeAccess;
	}

	/**
	 * Vergleich wird ermöglicht, wegen dem Comparable Interface.
	 */
	public int compareTo(LocaleWithWriteAccess o) {
		if (null == o || null == o.getLanguage())
			return 1;
		if (null == getLanguage())
			return -1;
		return Language.getLanguage().compareTo(o.getLanguage().getLanguage());
	}
}
