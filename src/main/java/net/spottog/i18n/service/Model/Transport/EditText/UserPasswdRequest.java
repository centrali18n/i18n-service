package net.spottog.i18n.service.Model.Transport.EditText;
/**
 * Ein Transport element, welches eine Benutzer id und ein Passwort enthält.
 */
public class UserPasswdRequest {
	private Integer userid;
	private String passwd;
	/**
	 * Das Passwort
	 * @return passwort
	 */
	public String getPasswd() {
		return passwd;
	}
	/**
	 * Das Passwort
	 * @param passwd passwort
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	/**
	 * Die User Id
	 * @return id
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * Die User id 
	 * @param userid id
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
}
