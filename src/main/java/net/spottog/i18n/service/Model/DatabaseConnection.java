package net.spottog.i18n.service.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
/**
 * Ein gecachtes Datenbank Objekt.
 * Es wird jede Datenbank Verbindung für eine Stunde vorgehalten.
 * Sind keine Datnebank verbindungen vorhanden, werden neue erzeugt.
 * Die Verwendung ist über einen try with resurces block vorgesehen.
 */
public class DatabaseConnection implements AutoCloseable {
	private static final List<DatabaseConnection> Save = Collections
			.synchronizedList(new LinkedList<DatabaseConnection>());
	private long expire = System.currentTimeMillis() + MAXAGEINMILLIS; // Damit nicht zu lange ungenutzte
																		// Datenbankverbindungen offen sind.
	private final static long MAXAGEINMILLIS = 3600000;
	private final Connection connection;
	private static boolean exit = false;
	/**
	 * Es wird eine neue Datenbank verbindung aufgegaut.
	 * Zugangsdaten werden über die dbConfig geregelt.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	private DatabaseConnection() throws SQLException {
		dbConfig config = dbConfig.getCurrent();
		connection = DriverManager.getConnection(config.getURL(), config.getPasswd(), config.getPasswd());
	}

	static {
		try {
			Class.forName(dbConfig.getCurrent().getClassname());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Das Datenbank objekt wird zurückgegeben.
	 */
	@Override
	public void close() throws SQLException {
		if (!exit && isValid()) {
			Save.add(this);
		} else {
			connection.close();
		}
	}
	/**
	 * Schließt sofort die Datenbank verbindung.
	 */
	private void ForceClose() {
		try {
			expire = 0;
			connection.close();
		} catch (SQLException e) {
			System.out.println("Error in DatabaseConnection: " + e.getMessage());
			e.printStackTrace();
		}
	}
	private boolean isValid() {
		return expire > System.currentTimeMillis();
	}
	/**
	 * see java.sql.Connection.prepareStatement
	 * @param sql see Connection.prepareStatement
	 * @param autoGeneratedKeys see Connection.prepareStatement
	 * @return see Connection.prepareStatement
	 * @throws SQLException see Connection.prepareStatement
	 */
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		return connection.prepareStatement(sql, autoGeneratedKeys);
	}
	/**
	 * see java.sql.Connection.prepareStatement
	 * @return see Connection.prepareStatement
	 * @throws SQLException see Connection.prepareStatement
	 */
	public Statement createStatement() throws SQLException {
		return connection.createStatement();
	}
	/**
	 * see java.sql.Connection.prepareStatement
	 * @param sql see Connection.prepareStatement
	 * @return see Connection.prepareStatement
	 * @throws SQLException see Connection.prepareStatement
	 */
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return connection.prepareStatement(sql);
	}
	/**
	 * Gibt ein Datenbank element zurück
	 * @return das Eatenbank element
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	public synchronized static DatabaseConnection Get() throws SQLException {
		if (exit)
			throw new IllegalStateException("Database was Close");
		if (Save.isEmpty())
			return new DatabaseConnection();
		return Save.remove(0);
	}
	/**
	 * Anzahl der gecachten Datenbank verbindungen.
	 * @return die zahl.
	 */
	public static int Size() {
		return Save.size();
	}
	/**
	 * Schließt Alle Datenbank Verbindungen und macht keine neuen mehr auf.
	 * Nur zum beenden der anwendung verwenden.
	 */
	public static void closeAll() {
		exit = true;
		while (!Save.isEmpty())
			Save.remove(0).ForceClose();
	}


}
