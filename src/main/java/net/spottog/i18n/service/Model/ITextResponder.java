package net.spottog.i18n.service.Model;

import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;
/**
 * Ist in der LAge einen TextWithLanguageRequest zu beantworten.
 */
public interface ITextResponder  {
	/**
	 * beantwortet einen TextWithLanguageRequest.
	 * z.b. indem in dem Dahinter liegenden Text Save gesucht wird.
	 * @param request die Anfrage.
	 * @return die Antwort.
	 */
	ITextWithLanguageAnswer search(final TextWithLanguageRequest request);
}
