package net.spottog.i18n.service.Model;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import net.spottog.i18n.global.ITextWithLanguageAnswer;
import net.spottog.i18n.global.TextWithLanguageRequest;
/**
 * Ein Ablaufenden Text.
 * kann  mit einem TextWithLanguageRequest ein ITextWithLanguageAnswer erzeugen.
 * die werte können nicht bearbeitet werden.
 * alle variablen sind final.
 */
public class ExpireText {
	private final String Text;
	private final int ExpireTime;
	/**
	 * Erzeugt einen ablaufenden text.
	 * @param Text der Text.
	 * @param ExpireTime das ablaufdatum.
	 */
	public ExpireText(final String Text, final int ExpireTime) {
		this.Text = Text;
		this.ExpireTime = ExpireTime;
	}
	/**
	 * Der Text
	 * @return als string
	 */
	public String getText() {
		return Text;
	}
	/**
	 * Die Zeit, bis der text abläuft.
	 * @return die Zeit
	 */
	public int getExpireTime() {
		return ExpireTime;
	}
	/**
	 * Erzeuge ein ITextWithLanguageAnswer element
	 * @param request für die antwort braucht es die frage
	 * @return die Antwort
	 */
	public ITextWithLanguageAnswer createAnswer(TextWithLanguageRequest request) {
		return new ITextWithLanguageAnswer() {

			@Override
			public String getSearchString() {
				return request.getSearchString();
			}

			@Override
			public Locale getLanguage() {
				return request.getLanguage();
			}

			@Override
			public String getText() {
				return Text;
			}

			@Override
			public Date getExpire() {
				if (getExpireTime() > 0) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.HOUR, getExpireTime());
					return cal.getTime();
				}
				//Wenn kleiner 0 kein ablauf
				return new Date(Long.MAX_VALUE);
			}
		};
	}

}
