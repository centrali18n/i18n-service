package net.spottog.i18n.service.Model;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import net.spottog.i18n.global.I18n;
import net.spottog.i18n.service.Controller.Controller;
import net.spottog.i18n.service.Controller.TextController;
/**
 * Ein Benutzer Objekt.
 * Stellt Methoden zur Benutzerverwaltung Bereit.
 */
public class User implements Serializable {
	private static final long serialVersionUID = 4126400206762944985L;
	private final Integer ID;
	private final String email;
	private String pwhash;
	private boolean edituser;
	private boolean editkeys;
	private boolean canbackup;
	private final LinkedList<TextRight> textRight = new LinkedList<>();
	private final ReadWriteLockAsResource textRightLock = new ReadWriteLockAsResource();
	/**
	 * Erzeugt ein Benutzer Objekt
	 * @param ID Die Eindeutige ID
	 * @param email die Email Adresse
	 * @param pwhash der Passwort Hash.
	 */
	public User(final Integer ID, final String email, final String pwhash) {
		this.ID = ID;
		this.email = email;
		this.pwhash = pwhash;
	}
	/**
	 * Die Eindeutige ID
	 * @return ID
	 */
	public int getID() {
		return ID;
	}
	/**
	 * Die Email Adressen
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Überprüft ob ein Klartext Passwrot zu dem Benutzer gehört.
	 * @param Passwd das Klartext Passwort
	 * @return true wenn es zu dem Gespeichertem Passwort gehört.
	 */
	public boolean CheckPasswd(final String Passwd) {
		return Controller.getInstance().getSecure().checkPasswd(Passwd, pwhash);
	}

	/**
	 * Set Password an return the Hash Value for the database. Please use the Method
	 * in the Controller to change it or Update the Datbase Manual
	 * Setzt das Passwort auf das Übergebene Passwort.
	 * Der Passwort eintrag in der Datenbank muss nachträglich geändert werden.
	 * Es Wird Empfohlen die Methode in dem UserController zu verwenden.
	 * Der UserController Führt seinerseits diese Methode aus, Speichert den zurückgegeben Hash aber gleich in der Datenbank.
	 * 
	 * @param passwd the cleartrext Password. Das Klartext Passwort
	 * @return the Hashed Password. Der Passwort Hash String.
	 */
	public String ChangePasswd(final String passwd) {
		pwhash = Controller.getInstance().getSecure().generatePW(passwd);
		return pwhash;
	}
	/**
	 * Fügt dem Benutzerobjekt lokal ein Benutzerrecht hinzu.
	 * Um ein Recht Duaerhaft zu vergeben (mit Datenbank) bitte die CreateNew MEthoden Verwenden.
	 * Ausnahme das Ändern von lese auf Schreibrechte.
	 * @param right Das benutzerrecht
	 * @param set muss true sein auser beim writeTet Right da bestimmt es ob schreibrechte.
	 * @param localeString darüber können Text rechte auf sprachen beschränkt werden
	 * @param SaveID der Textspeicher bei Text Berechtigungen.
	 * @throws SQLException  bei Datenbank Fehlern.
	 */
	public void addRightLocal(final UserRight right, boolean set, final String localeString, final int SaveID)
			throws SQLException {
		if (!set && right != UserRight.writeText)
			return;
		switch (right) {
		case editUser:
			edituser = true;
			break;
		case editKeys:
			editkeys = true;
			break;
		case backup:
			canbackup = true;
			break;
		case readText:
			set = false;
		case writeText:
			RemoveRight(set ? UserRight.writeText : UserRight.readText, localeString, SaveID);
			try (CloseableLock lock = textRightLock.writeLock()) {
				textRight.add(new TextRight(set, localeString, SaveID));
			}
		}
	}
	/**
	 * Entzieht einem Benutzer ein Benutzerrecht.
	 * Die Datenbank wird nicht bearbeitet.
	 * @param right das Benutzerrecht
	 * @param localeString die Sprache bei text rechten
	 * @param SaveID der Text Speicher
	 */
	public void RemoveRightLocal(UserRight right, String localeString, int SaveID) {
		LinkedList<TextRight> listToRemove = new LinkedList<>();
		switch (right) {
		case editUser:
			edituser = false;
			break;
		case editKeys:
			editkeys = false;
			break;
		case backup:
			canbackup = false;
			break;
		case readText:
			try(CloseableLock lock = textRightLock.writeLock()){
				for (TextRight tr : textRight) {
					if (tr.getSaveID() == SaveID
							&& (tr.canWrite() == (right == UserRight.writeText) &&( (localeString == null || localeString.trim().length() == 0 )) || (tr.getLocale() != null && tr.getLocale().equals(TextController.StringToLocale(localeString))))) {
							listToRemove.add(tr);
						}
					}
				}
				for (TextRight tr : listToRemove) {
					textRight.remove(tr);
				}
			break;
		case writeText:
			try(CloseableLock lock = textRightLock.writeLock()){
				for (TextRight tr : textRight) {
					if (tr.getSaveID() == SaveID
							&& ( localeString == null || localeString.trim().length() == 0 || ( tr.getLocale() == null && tr.getLocale() == TextController.StringToLocale(localeString)))) {
							listToRemove.add(tr);
						}
					}
				}
				for (TextRight tr : listToRemove) {
					textRight.remove(tr);
					if(!hasRight(UserRight.readText, tr.getLocale() == null ? "" : tr.getLocale().getLanguage(), tr.getSaveID())) {
					textRight.add(new TextRight(false, tr.getLocale(), tr.getSaveID()));
				}
			}
		}
	}
	/**
	 * Erzeugt eine Liste mit allen Text Speichern zu welchen der Benutzer mindestens Leserechte hat.
	 * @return Die Text Controller
	 */
	public Collection<TextController> getTextControllerList() {
		List<TextController> textcontroller = new LinkedList<>();
		for (Integer i : getSaveIDs()) {
			textcontroller.add(TextController.Get(i));
		}
		return textcontroller;
	}
	/**
	 * Erzeugt eine Liste mit den Ids aller Text Speicher zu welchen der Benutzer mindestens Leserechte hat.
	 * @return Die Id's
	 */
	public Collection<Integer> getSaveIDs() {
		Set<Integer> list = new TreeSet<>();
		try (CloseableLock lock = textRightLock.readLock()) {
			for (TextRight tr : textRight) {
				list.add(tr.getSaveID());
			}
			return list;
		}
	}
	/**
	 * Erzeugt eine Liste mit allen Sprechen, zu welchen der Benutzer Mindestens Lese rechte hat.
	 * Hat der Benutzer auf einen Text Speicher uneingeschränkte rechte werden alle Sprachen bei welchen der Text Speicher einen Eintrag hat der Liste hinzugefügt.
	 * @return Liste der Sprachen.
	 */
	public Collection<Locale> getLocaleList() {
		Set<Locale> ret = new HashSet<>();
		try (CloseableLock lock = textRightLock.readLock()) {
			for (TextRight tr : textRight) {
				if (tr.getLocale() == null)
					ret.addAll(TextController.Get(tr.getSaveID()).getLanguageList());
				else {
					ret.add(tr.getLocale());
				}
			}
		}
		return ret;
	}
	/**
	 * Erzeugt eine Auflistung mit allen Berechtigungen für einen bestimmten Text Speicher.
	 * @param saveid der Text speicher
	 * @return die auflistung aller Sprachen mit Berechtigung.
	 */
	public Collection<LocaleWithWriteAccess> getLocaleList(int saveid) {
		Set<LocaleWithWriteAccess> ret = new TreeSet<>();
		try (CloseableLock lock = textRightLock.readLock()) {
			for (TextRight tr : textRight) {
				if (tr.getSaveID() == saveid && tr.getLocale() == null)
					ret.add(new LocaleWithWriteAccess(null, tr.canWrite()));
				// TextController.Get(tr.getSaveID()).getLanguageList().forEach(l -> ret.add(new
				// LocaleWithWriteAccess(l, tr.canWrite())));
				else if (tr.getSaveID() == saveid) {
					ret.add(new LocaleWithWriteAccess(tr.getLocale(), tr.canWrite()));
				}
			}
		}
		return ret;
	}
	/**
	 * Überprüft ob ein Benutzer eine Bestimmte berechtigung hat.
	 * @param right das Benutzerrecht.
	 * @param localeString die Sprache bei text rechten
	 * @param SaveID der Text Save, wenn es sich um ein text recht handelt.
	 * @return true wenn der Benutzer das recht besitzt.
	 */
	public boolean hasRight(final UserRight right, final String localeString, final int SaveID) {
		switch (right) {
		case editUser:
			return edituser;
		case editKeys:
			return editkeys;
		case backup:
			return canbackup;
		case readText:
			try (CloseableLock lock = textRightLock.readLock()) {
				for (TextRight tr : textRight) {
					if (tr.getSaveID() == SaveID
							&& (tr.getLocale() == null || localeString == null || localeString.trim().length() == 0
									|| tr.getLocale().equals(TextController.StringToLocale(localeString))))
						return true;
				}
			}
			return false;
		case writeText:
			try (CloseableLock lock = textRightLock.readLock()) {
				for (TextRight tr : textRight) {
					if (tr.getSaveID() == SaveID
							&& (tr.getLocale() == null || localeString == null || localeString.trim().length() == 0
									|| tr.getLocale().equals(TextController.StringToLocale(localeString))))
						return tr.canWrite();
				}
			}
			return false;
		}
		return false;
	}
	/**
	 * Erzeugt dauerhaft ein Neues Benutzerrecht.
	 * @param right das Benutzerrecht
	 * @param localeString die sprache bei text rechten
	 * @param SaveID der Text Speicher bei text rechten.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	public void CreateNewRight(final UserRight right, String localeString, final int SaveID) throws SQLException {
		if (right == UserRight.readText || right == UserRight.writeText) {
			CreateNewTextRight(right, localeString, SaveID);
		} else {
			String sql = null;
			if (right == UserRight.editKeys) {
				sql = "Update \"User\" set editkeys = true WHERE \"ID\" = ?";
			} else if (right == UserRight.editUser) {
				sql = "Update \"User\" set edituser = true WHERE \"ID\" = ?";
			} else if (right == UserRight.backup) {
				sql = "Update \"User\" set backup = true WHERE \"ID\" = ?";
			}
			try (final DatabaseConnection con = DatabaseConnection.Get();
					final PreparedStatement st = con.prepareStatement(sql);) {
				st.setInt(1, ID);
				st.executeUpdate();
				addRightLocal(right, true, localeString, SaveID);
			}
		}
	}
	/**
	 * Erzeugt ein neues text recht.
	 * @param right das Benutzerrecht.
	 * @param localeString die sprache
	 * @param SaveID der Text Speicher.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	private void CreateNewTextRight(final UserRight right, String localeString, final int SaveID) throws SQLException {
		if (right != UserRight.readText && right != UserRight.writeText)
			throw new IllegalAccessError(new I18n("Userright is wrong").toString());
		else {
//			if (hasRight(UserRight.readText, localeString, SaveID)) {
//				System.out.println("Losche Recht.");
//				RemoveRight(UserRight.readText, localeString, SaveID);
//			}
//			if (right == UserRight.writeText && hasRight(UserRight.writeText, localeString, SaveID)) {
//				RemoveRight(UserRight.writeText, localeString, SaveID);
//			}
//			if (hasRight(right, localeString, SaveID)) {
//				RemoveRight(right, localeString, SaveID);
//			}
		}
		if (localeString == null || localeString.isEmpty())
			localeString = " ";
//		for (TextRight tr : textRight) {
//			if(tr.getSaveID() == SaveID && tr.canWrite() == (right == UserRight.writeText) && ((tr.getLocale() == null && localeString.equals(" ")) || (tr.getLocale() != null && tr.getLocale() == TextController.StringToLocale(localeString))))
//				RemoveRight(right, localeString, SaveID);
//		}
		String sql = "INSERT INTO \"userright\" (\"user\", \"language\", \"write\", \"SaveID\") VALUES (?, ?, ?, ?) ON CONFLICT (\"user\", \"language\", \"SaveID\") DO UPDATE SET \"write\" = ?";
		try (final DatabaseConnection con = DatabaseConnection.Get();
				final PreparedStatement st = con.prepareStatement(sql);) {
			int index = 0;
			st.setInt(++index, ID);
			st.setString(++index, localeString);
			st.setBoolean(++index, right == UserRight.writeText);
			st.setInt(++index, SaveID);
			st.setBoolean(++index, right == UserRight.writeText);
			st.executeUpdate();
			RemoveRightLocal(UserRight.readText, localeString, SaveID);
			addRightLocal(right, true, localeString, SaveID);
		}
	}

	/**
	 * Löscht dauerhaft und mit sofortiger wirkung ein Benutzerrecht.
	 * @param right das Benutzerrecht
	 * @param localeString die Sprache bei text rechten.
	 * @param SaveID der Text Speicher bei Text rechten.
	 * @throws SQLException bei Datenbank Fehlern.
	 */
	public void RemoveRight(UserRight right, String localeString, int SaveID) throws SQLException {
		String sqlupduser = null;
		switch (right) {
		case editUser:
			sqlupduser = "Update \"User\" set edituser = false WHERE \"ID\" = ?";
			break;
		case editKeys:
			sqlupduser = "Update \"User\" set editkeys = false WHERE \"ID\" = ?";
			break;
		case backup:
			sqlupduser = "Update \"User\" set backup = false WHERE \"ID\" = ?";
			break;
		case readText:
			for (TextRight tr : textRight) {
				if (tr.getSaveID() == SaveID
						&& (tr.getLocale() == null || localeString == null || localeString.trim().length() == 0
								|| tr.getLocale().equals(TextController.StringToLocale(localeString)))) {
					final String sql = "DELETE From \"userright\" WHERE \"user\"=? AND \"language\"=? AND \"SaveID\"=?";
					String langstr = " ";
					if (tr.getLocale() != null)
						langstr = tr.getLocale().getLanguage();
					try (final DatabaseConnection con = DatabaseConnection.Get();
							final PreparedStatement st = con.prepareStatement(sql);) {
						int index = 0;
						st.setInt(++index, ID);
						st.setString(++index, langstr);
						st.setInt(++index, tr.getSaveID());
						st.executeUpdate();
					}
				}
			}
			break;
		case writeText:
			try (CloseableLock lock = textRightLock.writeLock()) {
				for (TextRight tr : textRight) {
					if (tr.getSaveID() == SaveID
							&& (tr.getLocale() == null || localeString == null || localeString.trim().length() == 0
									|| tr.getLocale() == TextController.StringToLocale(localeString))) {
						final String sql = "UPDATE \"userright\" set \"write\"=false WHERE \"user\"=? AND \"language\"=? AND \"SaveID\"=?";
						String langstr = " ";
						if (tr.getLocale() != null)
							langstr = tr.getLocale().getLanguage();
						try (final DatabaseConnection con = DatabaseConnection.Get();
								final PreparedStatement st = con.prepareStatement(sql);) {
							int index = 0;
							st.setInt(++index, ID);
							st.setString(++index, langstr);
							st.setInt(++index, tr.getSaveID());
							st.executeUpdate();
						}
					}
				}
			}
		}
		if (sqlupduser != null) {
			try (final DatabaseConnection con = DatabaseConnection.Get();
					final PreparedStatement st = con.prepareStatement(sqlupduser);) {
				st.setInt(1, ID);
				st.executeUpdate();
			}
		}
		RemoveRightLocal(right, localeString, SaveID);
	}
	/**
	 * Entfernt alle Rechte für einen bestimmten text Save.
	 * Wirkt sich nur auf den Lokalen und nicht auf den Permanenten Speicher aus.
	 * @param saveID der Text Speicher.
	 */
	public void delAllRightLocal(int saveID) {
		try (CloseableLock lock = textRightLock.writeLock()) {
			textRight.removeIf(tr -> tr.getSaveID() == saveID);
		}
	}

}